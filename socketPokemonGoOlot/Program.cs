﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Connexio;
using System.Timers;
using System.Xml.Serialization;
using System.Xml;

namespace socketPokemonGoOlot
{
    class Program
    {
        private const string pokemonsFile = "auxFile.xml";
        private static Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); //Socket principal params
        private static readonly List<Socket> clientSockets = new List<Socket>(); // Llista de clients
        private const int BUFFER_SIZE = 2048; // mida max dels missatges 
        private const int PORT = 4444;
        private static byte[] buffer = new byte[BUFFER_SIZE]; //aqui arriba el missatge del client
        // Timer vairable
        private static System.Timers.Timer aTimer;
        // Entity variables
        public static pokemongoolotContext dbContext = new pokemongoolotContext();
        public static List<pokemonSalvatge> pokemonSalvatges = new List<pokemonSalvatge>();

        // Ferem un quadrat amb les coordenades
        public static double POSICIO_OLOT_MIN_LAT = 42.110420;
        public static double POSICIO_OLOT_MAX_LAT = 42.244882;
        public static double POSICIO_OLOT_MIN_LON = 2.434120;
        public static double POSICIO_OLOT_MAX_LON = 2.524120;

        static void Main(string[] args)
        {
            var decrypter = Decrypt.DecryptFile(); // => ENCRYPT / DECRYPT
            readPokemons(200);
            SetUp();
            SetTimer(); //Comença a resetejar els pokemons
            Console.ReadLine();
            aTimer.Stop(); // reset pokemons finish
            aTimer.Dispose();
        }

        private static void SetUp()
        {
            Console.WriteLine("Servidor escoltant el port : " + PORT);
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, PORT));
            serverSocket.Listen(10); // Peticions abans de tencar
            serverSocket.BeginAccept(AcceptCallback, null);
            Console.WriteLine("SERVER READY BEEEEECCCCCHHHH");
        }
        private static void AcceptCallback(IAsyncResult data)
        {
            Socket socket;
            try
            {
                socket = serverSocket.EndAccept(data);
            }
            catch (ObjectDisposedException)
            {
                Console.WriteLine("Connexion failed");
                return;
            }
            clientSockets.Add(socket); // ho afegim a la llista de clients
            socket.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, AdrecaCallback, socket);
            serverSocket.BeginAccept(AdrecaCallback, null); //Aqui accepta la peticio i crida a la funcio
        }

        private static void AdrecaCallback(IAsyncResult data)
        {
            Socket curent = (Socket)data.AsyncState; // agafem el client que demana la peticio
            int messageSize;

            try
            {
                messageSize = curent.EndReceive(data);
            }
            catch (SocketException)
            {
                Console.WriteLine("DISCONECTED");
                clientSockets.Remove(curent); // es treu de la llista de clients pk la peticio ha fallat
                curent.Close();
                return;
            }

            byte[] recive = new byte[messageSize];
            Array.Copy(buffer, recive, messageSize); // copiem la resposta per no alterar l'original (l'original hauria de ser readOnly)
            string path = Encoding.ASCII.GetString(recive);
            switch (path)
            { // AQUI ES ON ES GENEREN LES PETICIONS I EL QUE ENVIAR
                case "pokemons":
                    byte[] pokemons = Serialitzation();
                    sendData(pokemons, curent);
                    break;
                case "prova":
                    byte[] prova = Encoding.ASCII.GetBytes("Prova Conex");
                    sendData(prova, curent);
                    break;
                default:
                    Console.WriteLine("PATH NO IMPLEMENTAT");
                    break;
            }

            //Reactivem la peticio pk sino es tencaria a la primera
            curent.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, AdrecaCallback, curent);
        }

        public static void readPokemons(int number)
        {
            if (File.Exists(pokemonsFile))
            {
                System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(List<pokemonSalvatge>));
                System.IO.StreamReader file = new System.IO.StreamReader(pokemonsFile);
                pokemonSalvatges = (List<pokemonSalvatge>)reader.Deserialize(file);
                file.Close();
                if (pokemonSalvatges.Count > number) createPokemons(pokemonSalvatges.Count - number);

            }
            else
            {
                createPokemons(number);
            }
        }
        public static void createPokemons(int number)
        {
            for (int i = 0; i < number; i++)
            {
                pokemonSalvatges.Add(generateRandomPokemons());
            }
        }

        public static pokemonSalvatge generateRandomPokemons()
        {
            Random rnd = new Random();
            int rand = rnd.Next(1, 721);
            double randomLatitud = rnd.NextDouble() * (POSICIO_OLOT_MAX_LAT - POSICIO_OLOT_MIN_LAT) + POSICIO_OLOT_MIN_LAT;
            double randomLongitud = rnd.NextDouble() * (POSICIO_OLOT_MAX_LON - POSICIO_OLOT_MIN_LON) + POSICIO_OLOT_MIN_LON;

            List<Pokemon> pokemon = dbContext.Pokemons.Where(x => x.NumPokedex == rand).ToList();
            Pokemon pokemon1 = pokemon.First();
            pokemonSalvatge pokemonSalvatge =
                new pokemonSalvatge()
                {
                    BaseAttack = pokemon1.BaseAttack,
                    BaseDefense = pokemon1.BaseDefense,
                    ImgPokemon = pokemon1.ImgPokemon,
                    Latitud = randomLatitud,
                    Longitud = randomLongitud,
                    NomPokemon = pokemon1.NomPokemon,
                    NumPokedex = pokemon1.NumPokedex,
                    PcMax = pokemon1.PcMax,
                    PsMax = pokemon1.PsMax
                };
            return pokemonSalvatge;
        }

        private static void SetTimer()
        {
            // Reseteja cada 2 min 20 pokemons.
            aTimer = new System.Timers.Timer(120000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += resetPokemon;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void resetPokemon(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Pokemons Reset");
            Console.WriteLine(pokemonSalvatges.First().NomPokemon);
            pokemonSalvatges = pokemonSalvatges.Skip(20).ToList();
            createPokemons(20);
        }

        private static void sendData(byte[] data, Socket curent)
        {
            // Console.WriteLine(Encoding.ASCII.GetString(data));
            Console.WriteLine(data.Length);
            curent.Send(data); //Enviem les dades                 
        }

        public static byte[] Serialitzation() // passem la llista a xml i la convertim a byte[] ja que es l'unic valor que accepta el socket
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<pokemonSalvatge>));
            // Create an XmlTextWriter using a FileStream.
            Stream fs = new FileStream(pokemonsFile, FileMode.Create);
            XmlWriter writer = new XmlTextWriter(fs, Encoding.Unicode);
            // Serialize using the XmlTextWriter.
            serializer.Serialize(writer, pokemonSalvatges);

            writer.Close();

            string xmlString = System.IO.File.ReadAllText(pokemonsFile);
            byte[] prova = Encoding.ASCII.GetBytes(xmlString.ToString());

            return prova;
        }
    }
}




/* Cicle de vida del socket */
/*
    BeginRecive
    BeginAccept
    Send
    EndAccept / Remove 
*/
/* WebGrafia*/
/*
//Crear socket
    https://www.youtube.com/watch?v=x-mPC3KINrE&t=208s
    https://www.youtube.com/watch?v=xgLRe7QV6QI&t=1506s
    https://www.youtube.com/watch?v=WJU8cyNIvQE&t=10s

//OBJ
    https://www.youtube.com/watch?v=IDaQMcnZnvY
*/
/*
//Server send data
    https://stackoverflow.com/questions/19104242/how-to-send-a-message-from-the-server-to-a-client-using-sockets 
    https://stackoverflow.com/questions/14824491/can-i-communicate-between-java-and-c-sharp-using-just-sockets 

*/
/*
//Serialitzation / deserialitzation 
    https://www.codeproject.com/Articles/1163664/Convert-XML-to-Csharp-Object
*/

/* COORDENADES OLOT 

    42.245108, 2.372628

    42.056887, 2.327068

    42.049086, 2.660864

    42.308357, 2.649193
*/