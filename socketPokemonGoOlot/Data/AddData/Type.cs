﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Connexio
{
 class AddType  
 {

  public static List<Tipu> AddTypes(){
      List<Tipu> Types = new List<Tipu>();
      Types.Add(new Tipu() {NomTipus = "normal",NomColorHexadecimal="A8A77A"});
      Types.Add(new Tipu() {NomTipus = "fire",NomColorHexadecimal="EE8130"});
      Types.Add(new Tipu() {NomTipus = "water",NomColorHexadecimal="6390F0"});
      Types.Add(new Tipu() {NomTipus = "grass",NomColorHexadecimal="7AC74C"});
      Types.Add(new Tipu() {NomTipus = "electric",NomColorHexadecimal="F7D02C"});
      Types.Add(new Tipu() {NomTipus = "ice",NomColorHexadecimal="96D9D6"});
      Types.Add(new Tipu() {NomTipus = "fighting",NomColorHexadecimal="C22E28"});
      Types.Add(new Tipu() {NomTipus = "poison",NomColorHexadecimal="A33EA1"});
      Types.Add(new Tipu() {NomTipus = "ground",NomColorHexadecimal="E2BF65"});
      Types.Add(new Tipu() {NomTipus = "flying",NomColorHexadecimal="A98FF3"});
      Types.Add(new Tipu() {NomTipus = "psychic",NomColorHexadecimal="F95587"});
      Types.Add(new Tipu() {NomTipus = "bug",NomColorHexadecimal="A6B91A"});
      Types.Add(new Tipu() {NomTipus = "rock",NomColorHexadecimal="B6A136"});
      Types.Add(new Tipu() {NomTipus = "ghost",NomColorHexadecimal="735797"});
      Types.Add(new Tipu() {NomTipus = "dark",NomColorHexadecimal="705746"});
      Types.Add(new Tipu() {NomTipus = "dragon",NomColorHexadecimal="6F35FC"});
      Types.Add(new Tipu() {NomTipus = "steel",NomColorHexadecimal="B7B7CE"});
      Types.Add(new Tipu() {NomTipus = "fairy",NomColorHexadecimal="D685AD"});
      return Types;
  }   

 }   
}