﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Connexio
{
 class AddEvolucions  
 {

    public static List<Pokemon> AddEvolution( List<Pokemon> pokemons){
        int EvolutionsList = 475;
        // int EvolutionsList = 200;
        for (int i = 1; i <= EvolutionsList; i++)
        {
                Task<List<Pokemon>> poke = GetEvolutions(i,pokemons);
                pokemons = poke.Result;
        }

        return null;
    }  

    public static void addNomFamilia(string NomFamilia, int numero){
        pokemongoolotContext dbContext = new pokemongoolotContext();
        dbContext.Familia.Add(new Familia() {NomFamilia = NomFamilia, NumChain = numero});
        dbContext.SaveChanges();   
    }  

   public static async Task<List<Pokemon>> GetEvolutions(int numEspecie ,List<Pokemon> pokemons){
           pokemongoolotContext dbContext = new pokemongoolotContext();
           HttpClient client = new HttpClient();
           
            client.BaseAddress = new Uri("https://pokeapi.co/api/v2/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage apiCall = await client.GetAsync("https://pokeapi.co/api/v2/evolution-chain/"+numEspecie);
            if(apiCall.IsSuccessStatusCode){
                var json = await apiCall.Content.ReadAsStringAsync();
                EvolutionHelper Evoluitons = JsonConvert.DeserializeObject<EvolutionHelper>(json);
                Console.WriteLine(pokemons.FindIndex(item => item.NomPokemon.ToLower() == "shieldon"));
                Console.WriteLine(Evoluitons.chain.species.name);
                string NomEspecie = Evoluitons.chain.species.name;
                addNomFamilia(NomEspecie,numEspecie);
                Console.WriteLine(NomEspecie.ToLower() + "=> "+numEspecie);
                int pokemonInicial = pokemons.FindIndex(item => item.NomPokemon.ToLower() == NomEspecie.ToLower());
                Console.WriteLine(pokemonInicial);
                if(pokemonInicial != -1){
                pokemons[pokemonInicial].NomFaimlia = NomEspecie;
                pokemons[pokemonInicial].OrdreFamilia = 1;
                Console.WriteLine("ENTRA PRIMERA");
                foreach(var name in Evoluitons.chain.evolves_to ){
                   int i = 0;
                   int segona =  pokemons.FindIndex(item => item.NomPokemon.ToLower() == name.species.name.ToLower());
                   if(segona != -1){
                       pokemons[segona].NomFaimlia = NomEspecie;
                       Console.WriteLine("ENTRA SEGONA");
                       pokemons[segona].OrdreFamilia = 2 + i;  
                   }
                   
                    foreach( var nameTercera in Evoluitons.chain.evolves_to[i].evolves_to ){
                        int tercera =  pokemons.FindIndex(item => item.NomPokemon.ToLower() == nameTercera.species.name);
                        if(tercera != -1){
                            pokemons[tercera].NomFaimlia = NomEspecie;
                            Console.WriteLine("ENTRA TERCERA");
                            pokemons[tercera].OrdreFamilia = 3 + i;
                        }  
                    }
                }
                }
                Console.WriteLine("PASSA PER AQUI?");
                return pokemons;
            }else{
                return pokemons;
            }
        }  
    } 


}