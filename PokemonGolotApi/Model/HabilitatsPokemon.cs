﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;


namespace Connexio
{
    public partial class HabilitatsPokemon
    {
        [Key]
        [Column("num_pokedex")]
        public int NumPokedex { get; set; }
        [Key]
        [Column("nom_habilitat",TypeName ="varchar(40)")]
        public string NomHabilitat { get; set; }
        [JsonIgnore]
        public virtual Habilitat Habilitat { get; set; }
    }
}
