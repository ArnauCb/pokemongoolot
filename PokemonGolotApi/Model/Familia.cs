﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

#nullable disable

namespace Connexio
{
    public partial class Familia
    {   
        [Column("NomFamilia",TypeName ="varchar(40)")]
        public string NomFamilia {get; set;}

        [Column("num_pokedex")]
        public int NumChain{get;set;}
        [JsonIgnore]
        public virtual ICollection<Pokemon> Pokemons {get; set;}

    }
}
