﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using System.Text.Json.Serialization;


#nullable disable

namespace Connexio
{
    public partial class Pokemon
    {
        [Column("num_pokedex")]
        public int NumPokedex { get; set; }
        [Column("nom_pokemon",TypeName ="varchar(40)")]
        public string NomPokemon { get; set; }
        [Column("descripcio_pokemon",TypeName ="varchar(300)")]
        public string DescripcioPokemon { get; set; }
        [Column("img_pokemon",TypeName ="varchar(200)")]
        public string ImgPokemon { get; set; }
        [Column("tipus_pokemon",TypeName ="varchar(40)")]
        public string TipusPokemon { get; set; }
        [Column("ps_min")]
        public int? PsMax { get; set; }
        [Column("pc_min")]
        public int? PcMax { get; set; }
        [Column("defensa_base")]
        public int? BaseDefense {get ; set;}
        [Column("atac_base")]
        public int? BaseAttack {get ; set;}
        [Column("nom_faimilia",TypeName ="varchar(40)")]
        public string NomFaimlia {get; set;}
        [Column("ordre_familia")]
        public int OrdreFamilia {get; set;}
        [JsonIgnore]
        public virtual Tipu Tipus { get; set; }
        [NotMapped]
        [JsonIgnore]
        public virtual ICollection<HabilitatsPokemon> HabilitatsPokemons { get; set; }
        [NotMapped]
         [JsonIgnore]
        public virtual ICollection<UsuariPokemon> UsuariPokemons { get; set; }
        [NotMapped]
         [JsonIgnore]
        public virtual Familia Familia {get; set;}
        [NotMapped]
        [JsonIgnore]
        public virtual ICollection<PokedexUsuari> PokedexUsuaris { get; set; }

    }
}
