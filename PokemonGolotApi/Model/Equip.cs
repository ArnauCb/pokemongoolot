﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using System.Text.Json.Serialization;
#nullable disable

namespace Connexio
{
    public partial class Equip
    {

        [Key]
        [Column("nom_equip",TypeName ="varchar(30)")]
        public string NomEquip { get; set; }
         [Column("img_equip",TypeName ="varchar(100)")]
        public string ImgEquip { get; set; }
         [JsonIgnore]
        public virtual ICollection<Usuari> Usuaris { get; set; }

    }
}
