﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace PokemonGolotApi
{
     public partial class  ApiController : Controller {

       [HttpPost]
        [Route("usuari/login")]
        public async Task<Usuari> LoginUser([FromBody] Usuari user){
            Console.WriteLine("ENTRA LOGIN");
            if(user.Password != null && user.NomUsuari != null ){
                var myTask = Task.Run(() => dbContext.Usuaris.Where(m => m.NomUsuari == user.NomUsuari && m.Password == getHash(user.Password)).ToList());
                List<Usuari> usuari = await myTask;
                if(usuari.Count > 0){
                    Console.WriteLine("ENTRA TRUE");
                    return usuari.First();
                }else{
                    Console.WriteLine("ENTRA FALSE");
                    throw new NotImplementedException("Error "); 
                }
            }else{
                Console.WriteLine("ENTRA UN USUARI/PASSOWRD PATATUDO");
                 throw new Exception("NO USER/PASSWORD");
            }
        }

        // PROVA 2 FER-HO PER PARAMETRES (GET)
        [HttpGet]
        [Route("usuari/login/{NomUsuari}/{Password}")]
        public async Task<Usuari> LoginUserParams(string NomUsuari, string Password){
            Console.WriteLine("ENTRA LOGIN");
            Console.WriteLine(getHash(Password));
            if( Password != null && NomUsuari != null ){
                var myTask = Task.Run(() => dbContext.Usuaris.Where(m => m.NomUsuari == NomUsuari && m.Password == getHash(Password)).ToList());
                
                List<Usuari> usuari = await myTask;
                if(usuari.Count > 0){
                    Console.WriteLine("ENTRA TRUE");
                    Console.WriteLine(usuari.First());
                    return usuari.First();
                }else{
                    Console.WriteLine("ENTRA FALSE");
                    throw new NotImplementedException("Error "); // AIXO S'Ha de canviar

                }
            }else{
                Console.WriteLine("ENTRA UN USUARI/PASSOWRD PATATUDO");
                throw new NotImplementedException("Error "); // AIXO S'Ha de canviar
            }
        }


        [HttpGet]
        [Route("usuari/get/{nomUsuari}")]
        public async Task<Usuari> GetUsuari(string nomUsuari){
            Console.WriteLine("ENTRA GETUSER");
            var myTask = Task.Run(() => dbContext.Usuaris.Where(m => m.NomUsuari == nomUsuari).ToList());
            List<Usuari> usuari = await myTask;
            if(usuari.Count < 0){
                return null;
            }else{
                return usuari.First();
            }
        }

        [HttpGet]
        [Route("usuari/getAll")]
        public async Task<List<Usuari>> GetUsuaris(){
            var myTask = Task.Run(() => dbContext.Usuaris.ToList());
            List<Usuari> usuari = await myTask;
            if(usuari.Count < 0){
                return null;
            }else{
                return usuari;
            }
        }
        
        [HttpPost]
        [Route("usuari/add")]
        public void AddUser([FromBody] Usuari user){
            Console.WriteLine("ENTRA ADD");
            Console.WriteLine(user.Password);
           if(user.Password != null && user.NomUsuari != null){
                if(dbContext.Usuaris.Where(m => m.NomUsuari == user.NomUsuari).ToList().Count == 0){ // si hi ha un usuari amb el mateix nom
                    dbContext.Usuaris.Add(new Usuari {NomUsuari = user.NomUsuari,Password =getHash(user.Password),Distancia = user.Distancia,NomEquip=user.NomEquip,
                    Nivell =1.00,DataCreacio=user.DataCreacio,PolvosEstelares = user.PolvosEstelares});        
                    dbContext.SaveChanges();
                }else{
                   throw new Exception("USER REPEAT");
                }
            }else{
                Console.WriteLine(user.Password);
                Console.WriteLine(user.NomUsuari);
                  throw new Exception("NO PASSWORD");
            }
        }
        
       [ApiExplorerSettings(IgnoreApi=true)]
        public string getHash( string text){
            byte[] hashValue;
            UnicodeEncoding ue = new UnicodeEncoding();
            byte[] messageBytes = ue.GetBytes(text);
            SHA256 shHash = SHA256.Create();
            hashValue = shHash.ComputeHash(messageBytes);
            Console.WriteLine(System.Text.Encoding.UTF8.GetString(hashValue));
            return System.Text.Encoding.UTF8.GetString(hashValue);
        }


    }
}
