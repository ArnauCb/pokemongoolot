﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace PokemonGolotApi
{
     public partial class  ApiController : Controller {
        // PER QUAN GUARDEM IMATGES AL FORMULARI
        // [HttpPost]
        // [Route("pokeparada/add")]
        // public string AddPokeparada([FromForm] PokeparadaHelper pokeparada){
        //     Console.WriteLine("POKEPARAda");
        //     Console.WriteLine(pokeparada.Latitud);
        //     Console.WriteLine(pokeparada.Longitud);

        //     string imageName =""+ pokeparada.Latitud +"-"+pokeparada.Longitud +"-"+pokeparada.NomPokeparada+".png"; // Name per assegurar que sigui sempre unic
        //     var response = AddImage(pokeparada.ImgPokeparada,imageName);
        //     if( response != "Error"){
        //         Console.WriteLine(response);
        //         dbContext.Pokeparada.Add(new Pokeparada {Latitud = pokeparada.Latitud , Longitud = pokeparada.Longitud, 
        //             NomPokeparada = pokeparada.NomPokeparada,ImgPokeparada= response});        
        //         dbContext.SaveChanges();
        //         return "Ha entrat";
        //     }else{
        //        throw new Exception("NO IMAGE ADDED");
        //     }   
        // }

        [HttpPost]
        [Route("pokeparada/add")]
        public string AddPokeparada([FromBody] PokeparadaHelper pokeparada){
            Console.WriteLine("POKEPARAda");
            Console.WriteLine(pokeparada.Latitud);
            Console.WriteLine(pokeparada.Longitud);
                dbContext.Pokeparada.Add(new Pokeparada {Latitud = pokeparada.Latitud , Longitud = pokeparada.Longitud, 
                NomPokeparada = pokeparada.NomPokeparada,ImgPokeparada= null});        
                dbContext.SaveChanges();
                return "Ha entrat";   
        }
        
        [HttpGet]
        [Route("pokeparada/get/{Latitud}/{Longitud}")]
         public async Task<Pokeparada> GetPokeparada(double Latitud,double Longitud){
            var myTask = Task.Run(() => dbContext.Pokeparada.Where(m => m.Latitud == Latitud && m.Longitud == Longitud).ToList());
            List<Pokeparada> pokeparada = await myTask;
            if(pokeparada.Count < 0){
                return null;
            }else{
                return pokeparada.First();
            }
        }
        [HttpGet] // S'ha de provar <3
        [Route("pokeparada/getBetween/{Latitud}/{Latitud2}/{Longitud}/{Longitud2}")]
        public async Task<List<Pokeparada>> GetPokeparades(double LatitudMax, double LatitudMin,double LongitudMax,double LongitudMin){
            // Comprovar que les dades son fiables
            if(LatitudMax < LatitudMin){
                double aux = LatitudMax;
                LatitudMax = LatitudMin;
                LatitudMin = aux;
            }
            if(LongitudMax < LongitudMin){
                double aux = LongitudMax;
                LongitudMax = LongitudMin;
                LongitudMax = aux;
            }

            var myTask = Task.Run(()=> dbContext.Pokeparada.Where(m => (m.Latitud >= LatitudMax && m.Latitud <= LatitudMin) && (m.Longitud >= LongitudMax && m.Longitud <= LongitudMin)).ToList());
            List<Pokeparada> pokeparada = await myTask;
            if(pokeparada.Count < 0){
                return null;
            }else{
                return pokeparada;
            }
        }

        [HttpGet]
        [Route("pokeparada/getAll")]
        public async Task<List<Pokeparada>> GetAllPokeparada(string nomUsuari){
            var myTask = Task.Run(() => dbContext.Pokeparada.ToList());
            List<Pokeparada> pokeparada = await myTask;
            if(pokeparada.Count < 0){
                return null;
            }else{
                return pokeparada;
            }
        }
        [ApiExplorerSettings(IgnoreApi=true)]
        public string AddImage(IFormFile imatge,string FileName)
        {
            try
            {
              if(imatge.Length > 0)
              {
                  string currentDirectory = Environment.CurrentDirectory;
                  string path ="\\Imatges";
                  if(!Directory.Exists(path))
                  {
                      Directory.CreateDirectory(path);
                  }
                  string fileName = currentDirectory+ path+"\\"+FileName;
                  using (FileStream fileStream = System.IO.File.Create(fileName))
                  {
                      imatge.CopyTo(fileStream);
                      fileStream.Flush();  
                      return path+"\\"+FileName;
                  } 
              }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            throw new Exception("ERROR");
        }

    }
}
