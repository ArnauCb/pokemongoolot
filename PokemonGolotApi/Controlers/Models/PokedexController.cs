﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;
using Connexio;


namespace PokemonGolotApi
{
     public partial class  ApiController : Controller {

        [HttpGet]
        [Route("pokedex/get/{nomUsuari}/{limit}/{offset}")]
        public async Task<List<Pokemon>> GetPokedexPaginacio(string nomUsuari,int limit, int offset)
        {
            
             var pokemonsVist = Task.Run(()=> dbContext.PokedexUsuaris.Where((x) => x.NomUsuari == nomUsuari).ToList());
            List<PokedexUsuari> seenPokemon = await pokemonsVist;
           
            // List<PokedexUsuari> seenPokemon = new List<PokedexUsuari>();
            //seenPokemon.Add(new PokedexUsuari(){NomUsuari="admin",NumPokedex=1, DataVisualitzacio=DateTime.Now});
            // seenPokemon.Add(new PokedexUsuari(){NomUsuari="patata",NumPokedex=3});
            // seenPokemon.Add(new PokedexUsuari(){NomUsuari="patata",NumPokedex=17,DataVisualitzacio=new DateTime()});

            Console.WriteLine(seenPokemon[0].NumPokedex);
            var myTask = Task.Run(()=> dbContext.Pokemons.ToList());
            List<Pokemon> pokemons = await myTask;

            pokemons = pokemons.OrderBy((x) => x.NumPokedex).ToList();
            pokemons = pokemons.Skip(offset).Take(limit).ToList();

           // => PARAREL FOREACH
            int count = -1; //=> comença fent sumatorr
            Parallel.ForEach(pokemons,(u) =>
            {   
                //var currentCount = Interlocked.Increment(ref count); //per fer el count de la posicio

                var currentCount = Interlocked.Add(ref count, 1);
                Console.WriteLine("ENTRA A LA POSICIO =>"+currentCount);
               
                List<PokedexUsuari> pokedexUsuaris = seenPokemon.Where((x)=> x.NumPokedex == u.NumPokedex).ToList();

                if(pokedexUsuaris.Count > 0)// SI L'HA VIST
                {
                 if( pokedexUsuaris[0].CopsAtrapat > 0)
                 {
                     Console.WriteLine("S'HA ATRAPAT ");
                 }else
                 {
                     Console.WriteLine("NO S'HA ATRAPAT PERO L'HA VIST ");
                 }
                }else
                {
                    pokemons[currentCount].ImgPokemon = "https://images.wikidexcdn.net/mwuploads/wikidex/thumb/f/fa/latest/20200214190356/Unown_%3F_HOME.png/120px-Unown_%3F_HOME.png";
                }   
            });
                Console.WriteLine("MIDA =>"+ pokemons.Count());
                return (pokemons);
        }


    }
}
