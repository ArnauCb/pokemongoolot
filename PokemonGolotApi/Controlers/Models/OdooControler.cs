﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;

using PortaCapena.OdooJsonRpcClient;
using PortaCapena.OdooJsonRpcClient.Consts;
using PortaCapena.OdooJsonRpcClient.Converters;
using PortaCapena.OdooJsonRpcClient.Models;
using PortaCapena.OdooJsonRpcClient.Result;

using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;




namespace PokemonGolotApi
{
     public partial class  ApiController : Controller {
    
    public static OdooConfig config = new OdooConfig(
      apiUrl:"http://172.24.127.2:8069/",
      dbName:"odoodatabase1",
      userName:"pokegrup@gmail.com",
      password:"Patata123"
    );
    private OdooClient _odooClient = new OdooClient(config);
    private OdooConfig _odooConfig = config;

    
    private Task<OdooResult<int>> odooLogin()
    {
        return _odooClient.LoginAsync();
    }

    private async Task<OdooResult<Dictionary<string, OdooPropertyInfo>>> modelUserData() //=> Canviar user
    {
        return await _odooClient.GetModelAsync("res.users");
    }

    public class StadisticsUserMini
    {
        public string username { get; set; }
        public int hours_played { get; set; }
        public string email {get;set;}
    }


    [HttpGet]
    [Route("/odoo/getUserId/{id}")]
    public async Task<OdooResult<ResPartnerOdooModel>> getOdooUser(long id)
    {
        Console.WriteLine("ENTRA???");
        return await new OdooRepository<ResPartnerOdooModel>(_odooConfig).Query().Where(p => p.UserId, OdooOperator.EqualsTo, id).FirstOrDefaultAsync();
    }

    [HttpPost]
    [Route("/odoo/add")]
    public async Task  addUser([FromBody] StadisticsUserMini newUser)
    {
          
        var modl = OdooDictionaryModel.Create(() => new ResPartnerOdooModel()
        {
            DisplayName = newUser.username,
            Name = newUser.username,
            Totalhours = newUser.hours_played,
            Email = newUser.email,
            Active = true,
    
        });
        await new OdooRepository<ResPartnerOdooModel>(_odooConfig).CreateAsync(modl);
        OdooResult<ResPartnerOdooModel> resUsersOdooModel = await new OdooRepository<ResPartnerOdooModel>(_odooConfig).Query().Where(p=>p.Name, OdooOperator.EqualsTo, newUser.username).FirstOrDefaultAsync();
        Console.WriteLine(resUsersOdooModel.Id);
    }

    [HttpPut]
    [Route("/odoo/update")]
    public async Task<ActionResult<OdooResult<bool>>> updateUser([FromBody] StadisticsUserMini updateUser)
    {
        Console.WriteLine("ENTRA UPDATE");
        OdooResult<ResPartnerOdooModel> resUsersOdooModel = await new OdooRepository<ResPartnerOdooModel>(_odooConfig).Query().Where(p=>p.Name, OdooOperator.EqualsTo, updateUser.username).FirstOrDefaultAsync();
        if (resUsersOdooModel.Value.Id == null) { return NotFound(updateUser);}

        
        var model = OdooDictionaryModel.Create(() => new ResPartnerOdooModel()
        {
            DisplayName = updateUser.username,
            Name = updateUser.username,
            Totalhours = updateUser.hours_played,
            Email = updateUser.email,
            Active = true,
        });
        Console.WriteLine("Arrriba aqui UPDATE");
        return await new OdooRepository<ResPartnerOdooModel>(_odooConfig).UpdateAsync(model, resUsersOdooModel.Value.Id);
    }


    [HttpGet]
    [Route("/oddo/login")]
    public async Task<OdooResult<int>> GetLogin()
    {
        return await _odooClient.LoginAsync();
    }

    [HttpGet]
    [Route("/odoo/model/{table_name}")]
    public async Task<string> getModel(string table_name)
    {
        Console.WriteLine("ENTRA ODOO MODEL");
        await _odooClient.LoginAsync();
        Console.WriteLine("PASSA PER AQUI?");
        var modelResult = await _odooClient.GetModelAsync(table_name);
        return OdooModelMapper.GetDotNetModel(table_name, modelResult.Value);

    }

    }
}
