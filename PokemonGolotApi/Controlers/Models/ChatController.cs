﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;
using MongoDB.Driver;


namespace PokemonGolotApi
{  
     public partial class  ApiController : Controller {
         
        public  MongoClient client = new MongoClient("mongodb+srv://user:Patata44@cluster0.anjj9.mongodb.net/test"); 

         [HttpGet]
         [Route("hores/GetHores/{Usuari}")]
          public async Task<Hores> GetHores(string Usuari)
          {
            //=> CONNECTOR            
            var database =client.GetDatabase("provesChat");
            var provesChatDb = database.GetCollection<Hores>("MinutsUsuari");

            var myTask = Task.Run(() => provesChatDb.Find(d => d.NomUsuari.ToLower().Equals(Usuari.ToLower())).ToListAsync());
            //=> GET
            List<Hores> prova = await myTask;
            Console.WriteLine("MISSATGE");
            Console.WriteLine(prova.Count());
            Console.WriteLine(prova.First().NomUsuari);
            return prova.First();
          }
    
        [HttpPost]
        [Route("unity/postHores")]
        public void PostMessage([FromForm] Hores hores ){
            Console.WriteLine("Entra Missatge POST");
            Console.WriteLine(hores.NomUsuari);
            //=> CONNECTOR
            var database = client.GetDatabase("provesChat");
            var provesChatDb = database.GetCollection<Hores>("MinutsUsuari");

              //=> INSERT


            Hores message = new Hores() {MinutsTotals = hores.MinutsTotals, NomUsuari = hores.NomUsuari};
            provesChatDb.InsertOne(message);
        } 
    }         
}
