﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;


namespace PokemonGolotApi
{
     public partial class  ApiController : Controller {

        
        [HttpGet] 
        [Route("evolucio/get/{numFamilia}")]
        public async Task<List<Pokemon>> GetEvolucio(int numFamilia){
            // var myTask = Task.Run(() => dbContext.Pokemons.Where(m => m.NomFaimlia == nomFamilia).OrderBy(m => m.OrdreFamilia).ToList());
            var myTask = Task.Run(()=> dbContext.Pokemons.Join(dbContext.Familia, m => m.NomFaimlia, f => f.NomFamilia,(m,f) => new {m,f.NumChain}).Where(j=> j.NumChain == numFamilia).OrderBy(q => q.m.OrdreFamilia).ToArray());
            var List = await myTask;
            List<Pokemon> pokemon = new List<Pokemon>();
            foreach(var poke in List){ 
                pokemon.Add(poke.m);    
            }

            if(pokemon.Count < 0){
                throw new Exception("ERROR NO POKEMONS FOUND");
            }else{
                return pokemon;
            }
        }

        [HttpGet]
        [Route("evolucio/getByPokemon/{nomPokemon}")] 
        public async Task<List<Pokemon>> GetEvolucioByPokemon(string nomPokemon){
              var myTask = Task.Run(() => dbContext.Pokemons.Where(m => m.NomPokemon.ToLower() == nomPokemon.ToLower()).OrderByDescending(m => m.OrdreFamilia).ToList());
              List<Pokemon> pokemonFamilia = await myTask;
              if(pokemonFamilia.Count() > 0){
                string nomFamilia = pokemonFamilia[0].NomFaimlia;
                var myTask2 = Task.Run(() => dbContext.Pokemons.Where(m => m.NomFaimlia == nomFamilia).OrderBy(m => m.OrdreFamilia).ToList());
                List<Pokemon> pokemon = await myTask2;
                if(pokemon.Count > 0){
                    Console.WriteLine(pokemon[0].NomPokemon);
                    return pokemon;
                }else{
                   throw new Exception("ERROR NO POKEMONS FOUND");
                }
              }else{
                    throw new Exception("FAMILY NOT FOUND");
              }    
        }
    }
}
