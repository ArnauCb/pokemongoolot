﻿using System.Security.Authentication.ExtendedProtection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace PokemonGolotApi
{
     public partial class  ApiController : Controller {

        

        [HttpGet]
        [Route("pokemon/getAll")]
        public async Task<List<Pokemon>> GetAll() {
            var myTask = Task.Run(() => dbContext.Pokemons.ToListAsync());
            List<Pokemon> pokemon = await myTask;
            return pokemon;
        }
        // [HttpGet]
        // [Route("pokemon/get/{idPokemon}")]
        // public async Task<Pokemon> GetAll(int idPokemon) { 
        //     var myTask = Task.Run(()=> dbContext.Pokemons.Where(m => m.NumPokedex == idPokemon).ToList());
        //     List<Pokemon> pokemon = await myTask;
        //     if(pokemon.Count < 0){ // generar ErrorCode
        //         throw new Exception("ERROR");
        //     }
        //     return pokemon.First();
        // }
        [HttpGet]
        [Route("pokemon/getRange/{inici}/{fi}")]
        public async Task<List<Pokemon>> GetRangePokemons(int inici, int fi)
        {
            if(inici > fi){ //ordenar per si arriben malament
                int aux = fi;
                fi = inici;
                inici = aux;
            }

            var myTask = Task.Run(() => dbContext.Pokemons.OrderBy(p => p.NumPokedex).ToArray());
            Pokemon[] pokemon = await myTask;
            if( inici < 1){
               return pokemon.Take(fi).ToList(); 
            }else{
                return pokemon.Skip(inici - 1).Take(fi - (inici -1)).ToList();
            }
        }

        [HttpPost]
        [Route("pokemon/add")]
        public async void AddPokemon([FromBody] Pokemon pokemon){
            Console.WriteLine("ENTRA Crear pokemon");
            dbContext.Pokemons.Add(new Pokemon { NumPokedex= pokemon.NumPokedex, NomPokemon = pokemon.NomPokemon, ImgPokemon = Decrypt.EncryptFilePassVariable(Encoding.ASCII.GetBytes(pokemon.ImgPokemon)) , NomFaimlia = pokemon.NomFaimlia });
            dbContext.SaveChanges();

            Console.WriteLine("HEY HET");
        }
        
        [HttpGet]
        [Route("pokemon/get/{nom}")]
        public async Task<Pokemon> GetByName(string nom) { 
            var myTask = Task.Run(()=> dbContext.Pokemons.Where(m =>  m.NomPokemon.ToLower() == nom.ToLower()).ToList());
            List<Pokemon> pokemon = await myTask;
            if(pokemon.Count < 0){ // generar ErrorCode
                throw new Exception("ERROR");
            }
            pokemon.First().ImgPokemon = Decrypt.DecryptVar(Encoding.ASCII.GetBytes(pokemon.First().ImgPokemon));
            return pokemon.First();
        }
    }
}
