using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;
using System.IO;
using System.Security.Cryptography;
using System.Text;


namespace PokemonGolotApi
{
    public partial class ApiController : Controller
    {

        [HttpGet]
        [Route("pokedexUsuari/getPokedexByUser/{NomUsuari}")]
        public async Task<List<PokedexUsuari>> getPokedexUser(string NomUsuari)
        {
                var myTask = Task.Run(() => dbContext.PokedexUsuaris.Where((m)=> m.NomUsuari == NomUsuari).ToList());
                
                List<PokedexUsuari> llistaPokemons = await myTask;
               
                if (llistaPokemons.Count > 0)
                {
                    
                    return llistaPokemons;
                }
                else
                {
                    Console.WriteLine("ENTRA FALSE");
                    return null;

                }
            }
          
        }




    }
