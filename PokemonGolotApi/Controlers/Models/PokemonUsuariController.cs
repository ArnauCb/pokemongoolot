using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;
using System.IO;
using System.Security.Cryptography;
using System.Text;


namespace PokemonGolotApi
{
    public partial class ApiController : Controller
    {

        [HttpPost]
        [Route("pokemonUsuari/add/")]
        public async void capturarPokemon([FromBody] UsuariPokemon user){
            
                 dbContext.UsuariPokemons.Add(new UsuariPokemon {NomUsuari = user.NomUsuari,DataCaptura = DateTime.Now,NumPokedex = user.NumPokedex});        
                 dbContext.SaveChanges();

                 if(dbContext.PokedexUsuaris.Where(m => m.NumPokedex == user.NumPokedex).ToList().Count == 0){

                 dbContext.PokedexUsuaris.Add(new PokedexUsuari {NomUsuari = user.NomUsuari,DataVisualitzacio = DateTime.Now,NumPokedex = user.NumPokedex});
                 dbContext.SaveChanges();

                 }
                else
                {
                System.Console.WriteLine("Pokemon ja vist");
                }
                 
                
        }

        [HttpGet]
        [Route("pokemonUsuari/info/{id}")]
        public  async Task<List<UsuariPokemon> >  editPokemon(int id){
            
               var llista = dbContext.UsuariPokemons.Where(m => m.IdUsuariPokemon == id).ToList();
            return llista;
                 
                
        }

        [HttpPost]
        [Route("pokemonUsuari/createPokemon/")]
        public async void crearPokemon([FromBody] UsuariPokemon user){
            
                 dbContext.UsuariPokemons.Add(new UsuariPokemon {NomUsuari = user.NomUsuari,DataCaptura = DateTime.Now,NumPokedex = user.NumPokedex, Pc = user.Pc, Ps = user.Ps});        
                 dbContext.SaveChanges();

                 if(dbContext.PokedexUsuaris.Where(m => m.NumPokedex == user.NumPokedex).ToList().Count == 0){

                 dbContext.PokedexUsuaris.Add(new PokedexUsuari {NomUsuari = user.NomUsuari,DataVisualitzacio = DateTime.Now,NumPokedex = user.NumPokedex});
                 dbContext.SaveChanges();

                 }
                else
                {
                System.Console.WriteLine("Pokemon ja vist");
                }
                          
                
        }


        [HttpDelete]
        [Route("pokemonUsuari/delete/{id}")]
        public async void deletePokemon(int id){
            
                 dbContext.UsuariPokemons.Remove(dbContext.UsuariPokemons.Single((m)=> m.IdUsuariPokemon == id));        
                 dbContext.SaveChanges();

                
        }

        
        /*
        [HttpPut]
        [Route("pokemonUsuari/update/")]

        public async void updatePokemon([FromBody] UsuariPokemon){
            
                
                 if(dbContext.PokedexUsuaris.Where(m => m.NumPokedex == user.NumPokedex).ToList().Count == 0){

                 dbContext.PokedexUsuaris.Add(new PokedexUsuari {NomUsuari = user.NomUsuari,DataVisualitzacio = DateTime.Now,NumPokedex = user.NumPokedex});
                 dbContext.SaveChanges();

                 }
                else
                {
                System.Console.WriteLine("Pokemon ja vist");
                }
        }*/

        [HttpGet]
        [Route("pokemonUsuari/getAllPokemonsByUser/{NomUsuari}")]
        public async Task<List<UsuariPokemon>> getPokemonsUser(string NomUsuari)
        {
                var myTask = Task.Run(() => dbContext.UsuariPokemons.Where((m)=> m.NomUsuari == NomUsuari).ToList());
                
                List<UsuariPokemon> llistaPokemons = await myTask;
               
                if (llistaPokemons.Count > 0)
                {
                    
                    return llistaPokemons;
                }
                else
                {
                    Console.WriteLine("ENTRA FALSE");
                    return null;

                }
            }
          
        }




    }
