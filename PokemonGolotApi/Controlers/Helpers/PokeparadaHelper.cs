﻿using System;
using Microsoft.AspNetCore.Http;


namespace Connexio
{
    public partial class PokeparadaHelper
    {
       public IFormFile ImgPokeparada { get; set;}
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public string NomPokeparada { get; set; }
         
    }
}
