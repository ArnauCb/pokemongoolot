﻿using System;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace Connexio
{
    public partial class Combats
    {      
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id {get; set;}
        
        [BsonElement("player1")] //=> nom del camp
        public string Player1 {get;set;}
        
        [BsonElement("player2")] //=> nom del camp
        public string Player2 {get;set;}
        
        [BsonElement("winner")] //=> nom del camp
        public string Winner {get;set;}
    }
}
