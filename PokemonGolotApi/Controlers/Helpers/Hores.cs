﻿using System;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace Connexio
{
    public partial class Hores
    {      
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id {get; set;}
        
        [BsonElement("nomUsuari")] //=> nom del camp
        public string NomUsuari {get;set;}
        
        [BsonElement("minutsTotals")] //=> nom del camp
        public int MinutsTotals {get;set;}
        
    }
}
