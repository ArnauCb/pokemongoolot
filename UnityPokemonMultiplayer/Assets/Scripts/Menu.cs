﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


namespace DapperDino.Mirror.Tutorials.CharacterSelection
{
    public class Menu : MonoBehaviour
    {

        public NetworkManager networkManager;
        public GameObject menuPanel;
        bool paused = false;

        public void Host()
        {
            networkManager.StartHost();
            menuPanel.SetActive(false);
            paused = false;

        }

        public void SetIP(string ip)
        {
            networkManager.networkAddress = ip;
        }

        public void Join()
        {
            networkManager.StartClient();
            menuPanel.SetActive(false);
            paused = false;
        }

        public void Stop()
        {
            if (networkManager.mode == NetworkManagerMode.Host)
            {
                networkManager.StopHost();
            }
            if (networkManager.mode == NetworkManagerMode.ClientOnly)
            {
                networkManager.StopClient();
            }
            paused = false;
        }

        // Start is called before the first frame update
        void Start()
        {
            menuPanel.SetActive(true);
            paused = false;
        }

    }
}