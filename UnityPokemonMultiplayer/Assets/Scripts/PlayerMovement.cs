﻿using System.Security.Cryptography;
using Mirror;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DapperDino.Mirror.Tutorials.CharacterSelection
{
    public class PlayerMovement : NetworkBehaviour
    {
        [Header("References")]
        [SerializeField] private CharacterController controller = null;
        // [SerializeField] private Animator animator = null;

        [Header("Settings")]
        [SerializeField] private float movementSpeed = 5f;

        [Header("CHARACTER SPECIAL SHOOT")]
        [SerializeField] private GameObject specialShootObject ;

        [ClientCallback]
        private void Update()
        {
            if (!hasAuthority) { return; } //=> mirar si es el client
            var movement = new Vector3();

            if (Input.GetKey(KeyCode.W)) { movement.z += 1; }
            if (Input.GetKey(KeyCode.S)) { movement.z -= 1; }
            if (Input.GetKey(KeyCode.A)) { movement.x -= 1; }
            if (Input.GetKey(KeyCode.D)) { movement.x += 1; }
            if (Input.GetKeyDown(KeyCode.R)) { specialShoot(); }
            if (Input.GetKeyDown(KeyCode.Space)) { shoot(); }

            controller.Move(movement * movementSpeed * Time.deltaTime);

            if (controller.velocity.magnitude > 0.2f)
            {
                transform.rotation = Quaternion.LookRotation(movement);
            }

            // animator.SetBool("IsWalking", controller.velocity.magnitude > 0.2f);
        }

       
        [Command(requiresAuthority = false)]
        public void shoot( NetworkConnectionToClient sender = null)
	    {   
            GameObject pooledProjectile = ObjectPooler.SharedInstance.GetPooledObject();
            if (pooledProjectile != null)
            {
                pooledProjectile.SetActive(true); // activate it
                if(controller.transform.rotation.y <= 0)
                {
                    pooledProjectile.transform.position = new Vector3 (transform.position.x, 2 , transform.position.z + 1) ;
                }else
                {
                    pooledProjectile.transform.position = new Vector3 (transform.position.x, 2 , transform.position.z - 1) ;    
                }
                pooledProjectile.transform.rotation = controller.transform.rotation;
                pooledProjectile.GetComponent<MoveForward> ().speed = + 20;
                NetworkServer.Spawn(pooledProjectile, sender); 
            }
        }
        
        [Command(requiresAuthority = false)]
        public void  specialShoot(NetworkConnectionToClient sender = null)
        {
            Debug.Log("ENTRA SPECIAL");
            GameObject obj = Instantiate(specialShootObject);
            obj.transform.position = new Vector3 (transform.position.x, transform.position.y , transform.position.z ) ;
            NetworkServer.Spawn(obj, sender); 
            StartCoroutine(Despawn(5,obj));
        }

        bool coroutineRunning;
         IEnumerator Despawn(float seconds,GameObject obj) //=> Aqui hi va animacio de mort posar els segons =!
        {
           coroutineRunning=true;
            yield return new WaitForSeconds(seconds);
            if(isServer)
            {   
                Debug.Log("DESTROY IT");
                NetworkServer.Destroy(obj);
            }            
           coroutineRunning=false;
        }
    }
}
