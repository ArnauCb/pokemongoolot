﻿using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;


public class HealthBar : MonoBehaviour
{

	public Slider slider;
	public Gradient gradient;
	public Image fill;
	public int health = 100;

	void Start()
    {
      SetMaxHealth();  
    }
	
	void Update ()
	{
		SetHealth();	
	}


	public void SetMaxHealth()
	{
		// Debug.Log("HP =>>" + health);

		slider.maxValue = health;
		slider.value = health;
		// fill.color = gradient.Evaluate(1f);
	}

    public void SetHealth()
	{
				// Debug.Log("HP =>>" + health);

		slider.value = health;

		// fill.color = gradient.Evaluate(slider.normalizedValue);
	}

}
