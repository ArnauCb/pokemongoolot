﻿using System.Net.Mime;
using System.ComponentModel;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;

namespace DapperDino.Mirror.Tutorials.CharacterSelection
{
    public class CharacterSelect : NetworkBehaviour
    {
        [SerializeField] private GameObject characterSelectDisplay = default;
        [SerializeField] private Transform characterPreviewParent = default;
        [SerializeField] private TMP_Text characterNameText = default;
        [SerializeField] private Character[] characters = default;
        [SerializeField] private GameObject uidPlayer = default;
        [SerializeField] private List<Transform> spawnPoints = new List<Transform>();

        private int currentCharacterIndex = 0;
        public int currentPlayersConnected = 0;
        private List<GameObject> characterInstances = new List<GameObject>();

        public override void OnStartClient()
        {
            if (characterPreviewParent.childCount == 0)
            {
                foreach (var character in characters)
                {
                    GameObject characterInstance =
                        Instantiate(character.CharacterPreviewPrefab, characterPreviewParent);

                    characterInstance.SetActive(false);

                    characterInstances.Add(characterInstance);
                }
            }

            characterInstances[currentCharacterIndex].SetActive(true);
            characterNameText.text = characters[currentCharacterIndex].CharacterName;

            characterSelectDisplay.SetActive(true);
        }

        private void Update()
        {
        
        }


          public void Select()
        {
            CmdSelect(currentCharacterIndex); 
            characterSelectDisplay.SetActive(false);
            //uidPlayer.SetActive(true);
            GameObject.Find("AudioMain").GetComponent<AudioSource>().mute = true; 
             

            GameObject PlayerBar1 = uidPlayer.transform.Find("Health barPlayer1").gameObject;
            GameObject PlayerBar2 = uidPlayer.transform.Find("Health barPlayer2").gameObject;

            PlayerBar1.SetActive(true);
            PlayerBar2.SetActive(true);


            Debug.Log(NetworkServer.connections.Count); //=> FER AIXO PER SPAWNS DINAMICS
        }


        [Command(requiresAuthority = false)]
         public void CmdSelect(int characterIndex, NetworkConnectionToClient sender = null)
        {
            GameObject characterInstance = Instantiate(characters[characterIndex].GameplayCharacterPrefab); //=> Parse gameObject 
            characterInstance.transform.position = spawnPoints[currentPlayersConnected].position;
            currentPlayersConnected++;
            NetworkServer.Spawn(characterInstance, sender);
        }

        public void Right()
        {
            characterInstances[currentCharacterIndex].SetActive(false);

            currentCharacterIndex = (currentCharacterIndex + 1) % characterInstances.Count;

            characterInstances[currentCharacterIndex].SetActive(true);
            characterNameText.text = characters[currentCharacterIndex].CharacterName;
        }

        public void Left()
        {
            characterInstances[currentCharacterIndex].SetActive(false);

            currentCharacterIndex--;
            if (currentCharacterIndex < 0)
            {
                currentCharacterIndex += characterInstances.Count;
            }

            characterInstances[currentCharacterIndex].SetActive(true);
            characterNameText.text = characters[currentCharacterIndex].CharacterName;
        }
    }
}
