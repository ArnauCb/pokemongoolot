using System.Threading;
using System;
using Mirror;
using UnityEngine.Networking;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using System.Linq;

namespace DapperDino.Mirror.Tutorials.CharacterSelection
{

    public class PlayerHit : NetworkBehaviour
    {
        public List<Events> events = new List<Events>(); //=> Sempre private 
        public List<Events> eventsEnemy = new List<Events>();


        [SerializeField]
        private int Hp = 100;
        [SerializeField]
        private int damage = 10;



        [SyncVar(hook = nameof(SetNewHp))]
        private int currentHealth = 100;

        public void SetNewHp(int old, int newHp)
        {


            if (newHp < old)
            {
                GameObject.Find("AudioPokemonDmg").GetComponent<AudioSource>().Play();
            }
            else
            {
                GameObject.Find("AudioPokemonHp").GetComponent<AudioSource>().Play();
            }
            if (!hasAuthority)
            {
                SetHealth(newHp);
            }
            else
            {
                SetHealthOtherPlayer(newHp);
            }
        }
        //=> CREAR API REST POST MONGO
        [ServerCallback]
        public IEnumerator SendData(int winner)
        {
            Debug.Log("ENTRA HERE ENTRAR DATA!!");
            WWWForm form = new WWWForm();
            form.AddField("Player1", "Charmander");
            form.AddField("Player2", "Charmander");
            form.AddField("Winner", winner);

            UnityWebRequest www = UnityWebRequest.Post("http://172.24.4.244:5000/unity/setWinner", form);
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("HERE ERROR");
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }


        void Update()
        {

            if (currentHealth <= 0)
            {

                if (hasAuthority)
                {
                    GameObject uidPlayer = GameObject.FindGameObjectWithTag("Canvas");
                    GameObject defeat = uidPlayer.transform.Find("Loser").gameObject;
                    defeat.SetActive(true);

                }
                else
                {
                    GameObject uidPlayer = GameObject.FindGameObjectWithTag("Canvas");
                    GameObject winner = uidPlayer.transform.Find("Winner").gameObject;
                    winner.SetActive(true);
                }

                if (!coroutineRunning)
                    StartCoroutine(Dead(1));
            }
        }

        bool coroutineRunning;
        IEnumerator Dead(float seconds) //=> Aqui hi va animacio de mort posar els segons =!
        {
            if (hasAuthority) 
            {
                StartCoroutine(SendData(1));
            }
            else
            {
                StartCoroutine(SendData(2));
            }
            coroutineRunning = true;
            yield return new WaitForSeconds(seconds);
            NetworkServer.Destroy(gameObject); // => quan acabi es borra
            coroutineRunning = false;
        }

        void Start()
        {
            SetMaxHealth();

        }

        void OnTriggerEnter(Collider shoot)
        {
            Debug.Log("ENTRA ????");
            if (shoot.tag == "shoot")
            {
                if (gameObject.tag == "Player")
                {
                    if (isServer)
                    {
                        Debug.Log("LA VIDA BAIXA !!!");
                        currentHealth = Mathf.Max(currentHealth - damage, 0);
                    }
                }
            }
            if (shoot.tag == "PoweUp")
            {
                Debug.Log("Ha agafat PowerUp");
                if (currentHealth >= Hp - 10)
                {
                    currentHealth = Hp;

                }
                else
                {
                    currentHealth += 10;

                }
                if (!hasAuthority)
                {
                    SetHealth(currentHealth);
                }
                else
                {
                    SetHealthOtherPlayer(currentHealth);
                }
                NetworkServer.Destroy(shoot.gameObject);
            }
        }


        public void SetMaxHealth()
        {
            GameObject HpPlayerBar = GameObject.Find("Health barPlayer1");
            Debug.Log(HpPlayerBar.GetComponent<Slider>().maxValue);
            Slider slider = HpPlayerBar.GetComponent<Slider>();
            Image fill = HpPlayerBar.GetComponent<Image>();
            slider.maxValue = Hp;
            slider.value = Hp;

            // Init player2
            GameObject HpPlayerBar2 = GameObject.Find("Health barPlayer2");
            Slider slider2 = HpPlayerBar2.GetComponent<Slider>();
            Image fill2 = HpPlayerBar2.GetComponent<Image>();
            slider2.maxValue = Hp;
            slider2.value = Hp;

        }

        public void SetHealth(int hp)
        {
            Debug.Log("DAMAGE DEAL  PLAYER : =>" + hp);
            GameObject HpPlayerBar = GameObject.Find("Health barPlayer1");
            Slider slider = HpPlayerBar.GetComponent<Slider>();
            Image fill = HpPlayerBar.GetComponent<Image>();
            //  Gradient gradient = HpPlayerBar.GetComponent<Gradient>();
            slider.value = hp;
            if (isServer)
            {
                if (hasAuthority)
                {
                    Debug.Log("ENTRA AUTORITAT");

                }
                else
                {
                    Debug.Log("ENTRA NO AUTORITAT");
                    SetData(hp, 10, 1);
                }
            }
        }

        public void SetHealthOtherPlayer(int hp)
        {
            Debug.Log("DAMAGE DEAL OTHER PLAYER : =>" + hp);
            GameObject HpPlayerBar2 = GameObject.Find("Health barPlayer2");
            Slider slider = HpPlayerBar2.GetComponent<Slider>();
            Image fill = HpPlayerBar2.GetComponent<Image>();
            //  Gradient gradient = HpPlayerBar.GetComponent<Gradient>();
            slider.value = hp;
            if (isServer)
            {
                if (!hasAuthority)
                {
                    Debug.Log("ENTRA NO AUTORITAT");

                }
                else
                {
                    Debug.Log("ENTRA AUTORITAT");
                    SetDataEnemy(hp, 10, 2);
                }
            }
        }


        [Command]
        public void SetDataEnemy(int Hp, int diferencia, int player)
        {
            eventsEnemy.Add(new Events { player = player, playerHp = Hp, diferencia = diferencia });
            Debug.Log("NUM DE EVENTS =>" + events.Count);
            Debug.Log("NUM DE EVENTS ENEMIC =>" + eventsEnemy.Count);
            Debug.Log("SOC SERVER =>" + isServer);
        }

        [Command]
        public void SetData(int Hp, int diferencia, int player)
        {
            events.Add(new Events { player = player, playerHp = Hp, diferencia = diferencia });
            Debug.Log("NUM DE EVENTS =>" + events.Count);
            Debug.Log("NUM DE EVENTS ENEMIC =>" + eventsEnemy.Count);
            Debug.Log("SOC SERVER =>" + isServer);
        }
    }
}




/*
 Debug.Log("EVENTS PLAYER STARTED EVENT LENGTH =>"+events.Count);
                foreach (Events item in events)
                {
                   Debug.Log(""+item.player+" =>"+item.playerHp); 
                }

                Debug.Log("EVENTS ENEMY STARTED EVENT LENGTH =>"+eventsEnemy.Count);
                foreach (Events enemy in eventsEnemy)
                {
                   Debug.Log(""+enemy.player+" =>"+enemy.playerHp); 
                }
*/


// // Start is called before the first frame update
// void Start()
// {

// }

// void OnTriggerEnter(Collider shoot)
// {
//     if(shoot.tag == "shoot")
//     {
//         shoot.gameObject.SetActive(false);
//         Hp -= 10;
//             if(gameObject.tag == "Player")
//             {
//                 Hp = Hp - 10;
//                 Debug.Log("HP =>"+Hp);
//                 // var playerObjectProva = GameObject.Find("Health barRed");
//                 // var script = playerObjectProva.GetComponent<HealthBar>().health = Hp ;
//             }
//             if(gameObject.tag == "player2")
//             {
//                 // var playerObjectProva = GameObject.Find("Health barBlue");
//                 // var script = playerObjectProva.GetComponent<HealthBar>().health = Hp ;
//             }


//         if(shoot.tag == "player2")
//         {
//             //  var playerObject = GameObject.Find("Health barBlue");
//         }

//     if(Hp <= 0)
//     {
//         gameObject.SetActive(false);
//     }
// }
// }