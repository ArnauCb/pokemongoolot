﻿using System.Security.Cryptography;
using Mirror;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DapperDino.Mirror.Tutorials.CharacterSelection
{
    public class SpawnPowerUp : NetworkBehaviour
    {
        [SerializeField] private List<Transform> spawnPoints = new List<Transform>();
        [SerializeField] private List<GameObject> powerUps = new List<GameObject>();

        // [Command]
        private void Start()
        { 
           StartCoroutine(Dead(2));
        }
        bool coroutineRunning;
         
         IEnumerator Dead(float seconds) //=> Aqui hi va animacio de mort posar els segons =!
        {
           int RandomPowerUp = Random.Range(0, powerUps.Count);
           int RandomSpawnPoint = Random.Range(0, spawnPoints.Count);
           int NextPowerUp = Random.Range(4,12);
           coroutineRunning=true;
            yield return new WaitForSeconds(seconds);
            //=> Start funcion after timer 
            Debug.Log("GENERA POWER UP");
            Debug.Log("RANDOM SPAWN =>"+RandomSpawnPoint + " RANDOM POWER UP => "+RandomPowerUp);
            GameObject obj = Instantiate(powerUps[RandomPowerUp]);
             obj.transform.position = spawnPoints[RandomSpawnPoint].position;
            CmdSpawn(obj);
           

            coroutineRunning=false;
            StartCoroutine(Dead(NextPowerUp)); // => Crida recursiva
        }

        [Command]
        public void CmdSpawn(GameObject obj,NetworkConnectionToClient sender = null)
        {
            Debug.Log("SPAWN POWER UP");
             NetworkServer.Spawn(obj, sender); 
        }
    }
}
