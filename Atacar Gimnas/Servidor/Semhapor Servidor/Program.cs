﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

class Program
{
    static readonly object _lock = new object();
    static readonly Dictionary<int, TcpClient> list_clients = new Dictionary<int, TcpClient>();
    static int vida = 100;
    static int count = 0;
    static void Main(string[] args)
    {


        TcpListener ServerSocket = new TcpListener(IPAddress.Any, 5000);
        ServerSocket.Start();
        Console.WriteLine("Server Engegat!!");
        while (true)
        {
            TcpClient client = ServerSocket.AcceptTcpClient();
            if (count < 10)
            {
            lock (_lock) list_clients.Add(count, client);
            Console.WriteLine("Someone connected!!");

            Thread t = new Thread(handle_clients);
            t.Start(count);
            count++;
                ple("T'has connectat correctament", client);
            }
            else
            {
                ple("El servidor esta ple", client);
            }
            Console.WriteLine("Numero de Clients Connectats "+ count);
        }
    }

    public static void handle_clients(object o)
    {
        int id = (int)o;
        TcpClient client;

        lock (_lock) client = list_clients[id];

        while (true)
        {
            NetworkStream stream = client.GetStream();
            byte[] buffer = new byte[1024];
            int byte_count = stream.Read(buffer, 0, buffer.Length);

            if (byte_count == 0)
            {
                break;
            }

            string data = Encoding.ASCII.GetString(buffer, 0, byte_count);
            if (data.Equals("Atack"))
            {
                vida = vida - 100;
                Console.WriteLine("La vida del Pokemon ha disminuit a = " + vida.ToString());
            }
            string resposta;
            if (vida <= 0)
            {
                vida = 0;
                Console.WriteLine("El Pokemon s'ha debilitat");
                 resposta = "El Pokemon s'ha debilitat";
            }
            else
            {
                resposta = "Vida Pokemon = " + vida.ToString();
            }
            broadcast(resposta);
        }
        count--;
        Console.WriteLine("Un client s'ha desconectat");
        Console.WriteLine("Numero de Clients Connectats " + count);
        lock (_lock) list_clients.Remove(id);
        client.Client.Shutdown(SocketShutdown.Both);
        client.Close();
    }

    public static void broadcast(string data)
    {
        byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);

        lock (_lock)
        {
            foreach (TcpClient c in list_clients.Values)
            {
                NetworkStream stream = c.GetStream();

                stream.Write(buffer, 0, buffer.Length);
            }
        }
    }
    public static void ple(String data ,TcpClient client)
    {
        byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);

                NetworkStream stream = client.GetStream();

                stream.Write(buffer, 0, buffer.Length); 
        
    }
}