﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AplicacióWPFPokemonGoOlot.PokemonsUsuariInfo;


namespace AplicacióWPFPokemonGoOlot.EditarPokemonUsuari
{
    /// <summary>
    /// Lógica de interacción para EditarPokemonUsuari.xaml
    /// </summary>
    public partial class EditarPokemonUsuari : Window
    {
        public EditarPokemonUsuari()
        {
            InitializeComponent();
            getInfoUsuari();
        }

        public async Task getInfoUsuari()
        {
            int id = GlobalPokemonUsuari.get();
            List<PokemonsUsuariModel> List = await PokemonsUsuariProcessor.GetInfoUser(id);
            txtPc.Text = List[0].Pc.ToString();
            txtPs.Text = List[0].Ps.ToString();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GestionarPokemonsUsuaris.GestionarPokemonsUsuari menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }
    }
}
