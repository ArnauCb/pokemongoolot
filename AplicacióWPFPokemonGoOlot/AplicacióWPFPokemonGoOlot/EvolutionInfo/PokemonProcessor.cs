﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AplicacióWPFPokemonGoOlot.EvolutionInfo
{
    internal class PokemonProcessor
    {

         public static string baseApi = $"http://172.24.4.244:5000/";
        //public static string baseApi = $"http://127.0.0.1:5000/";
        public static async Task<List<PokemonModel>> LoadPokemon(string namePokemon)
        {

            string url = "";
            url = baseApi + "evolucio/getByPokemon/" + namePokemon ;


            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    List<PokemonModel> model = await response.Content.ReadAsAsync<List<PokemonModel>>();


                    return model;
                }
                else
                {
                   // throw new Exception("PAtata");
                    return null;
                }


            }
        }

    }
}
