﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacióWPFPokemonGoOlot.EvolutionInfo
{
    internal class PokemonModel
    {


        public int NumPokedex { get; set; }
      
        public string NomPokemon { get; set; }
      
        public string DescripcioPokemon { get; set; }
   
        public string ImgPokemon { get; set; }
  
        public string TipusPokemon { get; set; }
  
        public int? PsMax { get; set; }
    
        public int? PcMax { get; set; }
       
        public int? BaseDefense { get; set; }
    
        public int? BaseAttack { get; set; }
    
        public string NomFaimlia { get; set; }
        public int OrdreFamilia { get; set; }

    }
}
