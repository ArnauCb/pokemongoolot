﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AplicacióWPFPokemonGoOlot.PokemonsUsuariInfo;

namespace AplicacióWPFPokemonGoOlot.CrearPokemonUsuari
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }
        public async Task CreatePokemon()
        {
            //int id = Convert.ToInt32(txtId.Text);
            int NumPokedex = Convert.ToInt32(txtNumPokedex.Text);
            string NomUsuari = txtUsuari.Text;
            DateTime DataCaptura = Convert.ToDateTime(txtDataCaptura.Text);
            int Pc = Convert.ToInt32(txtPc.Text);
            int Ps = Convert.ToInt32(txtPs.Text);
            PokemonsUsuariModel usuari = new PokemonsUsuariModel() {NumPokedex = NumPokedex, NomUsuari = NomUsuari, DataCaptura = DataCaptura, Pc = Pc, Ps = Ps };
           
            var PokemonUsuariInfo = await PokemonsUsuariProcessor.AddPokemonUser(usuari);

            if (PokemonUsuariInfo == true)
            {
                MessageBox.Show("Pokemon creat correctament");
                txtNumPokedex.Text = "";
                txtUsuari.Text = "";
                txtDataCaptura.Text = "";
                txtPc.Text = "";
                txtPs.Text = "";
            }
            else
            {
                MessageBox.Show("El Pokemon no s'ha pogut crear");
            }

        }
        private void Button_MouseRight(object sender, RoutedEventArgs e)
        {
            GestionarPokemonsUsuaris.GestionarPokemonsUsuari menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }

        private async void enviarPokemon(object sender, RoutedEventArgs e)
        {


            if (txtNumPokedex.Text == "" || txtUsuari.Text == "" || txtDataCaptura.Text == "" || txtPc.Text == "" || txtPs.Text == "")
            {
                MessageBox.Show("Omple els camps buits abans de crear el Pokemon.");
            }
            else
            {

                await CreatePokemon();

            }


        }


    }
}
