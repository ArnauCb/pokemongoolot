﻿#pragma checksum "..\..\..\CadenaEvolucio.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "308B56D8A1FD95552974175F24063D9F1AACDC34"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AplicacióWPFPokemonGoOlot {
    
    
    /// <summary>
    /// CadenaEvolucio
    /// </summary>
    public partial class CadenaEvolucio : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 104 "..\..\..\CadenaEvolucio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Poke3;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\CadenaEvolucio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Ev3;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\CadenaEvolucio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Poke1;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\CadenaEvolucio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Ev1;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\..\CadenaEvolucio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Poke2;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\CadenaEvolucio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Ev2;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AplicacióWPFPokemonGoOlot;V1.0.0.0;component/cadenaevolucio.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\CadenaEvolucio.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\..\CadenaEvolucio.xaml"
            ((AplicacióWPFPokemonGoOlot.CadenaEvolucio)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Arrosegar);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Poke3 = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.Ev3 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.Poke1 = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.Ev1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.Poke2 = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.Ev2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

