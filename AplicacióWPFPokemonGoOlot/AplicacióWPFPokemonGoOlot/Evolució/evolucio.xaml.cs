﻿using AplicacióWPFPokemonGoOlot.EvolutionInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplicacióWPFPokemonGoOlot.Evolució
{
    /// <summary>
    /// Lógica de interacción para Evolucio.xaml
    /// </summary>
    public partial class Evolucio : Window
    {


        public Evolucio()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }



        int total;

        public async Task<int> getPokemonData(int count)
        {
            var pokemon = PokeCerca.Text;

            List<PokemonModel> EvolutionInfo = await PokemonProcessor.LoadPokemon(pokemon);

            if (EvolutionInfo == null)
            {
                //Console.WriteLine("Error.");

            }
            else
            {

                // MessageBox.Show(EvolutionInfo[0].NomPokemon);

                total = EvolutionInfo.Count;

                foreach (PokemonModel pokemonEV in EvolutionInfo)
                {
                    count++;


                    switch (count)
                    {
                        case 1:

                            Poke1.Source = new BitmapImage(new Uri(pokemonEV.ImgPokemon));
                            Ev1.Text = pokemonEV.NomPokemon;

                            break;

                        case 2:

                            Poke2.Source = new BitmapImage(new Uri(pokemonEV.ImgPokemon));
                            Ev2.Text = pokemonEV.NomPokemon;

                            break;

                        case 3:

                            Poke3.Source = new BitmapImage(new Uri(pokemonEV.ImgPokemon));
                            Ev3.Text = pokemonEV.NomPokemon;

                            break;

                        default:

                            //MessageBox.Show("Error");

                            break;
                    }
                }
            }

            return total;

        }

        private async void Evo_Click(object sender, RoutedEventArgs e)
        {

            Poke1.Source = new BitmapImage(new Uri("pack://application:,,,/Evolució/EvolutionInterrogative.png"));
            Ev1.Text = "Evolució 1";

            Poke2.Source = new BitmapImage(new Uri("pack://application:,,,/Evolució/EvolutionInterrogative.png"));
            Ev2.Text = "Evolució 2";

            Poke3.Source = new BitmapImage(new Uri("pack://application:,,,/Evolució/EvolutionInterrogative.png"));
            Ev3.Text = "Evolució 3";

            if (PokeCerca.Text == "")
            {
                MessageBox.Show("Omple el camp de cerca abans de prémer el botó.");
            }
            else
            {
                await getPokemonData(0);
            }

            if (total > 3)
            {

                SliderPokemon.Visibility = Visibility.Visible;
                SliderPokemon.Minimum = 0;
                SliderPokemon.Maximum = total - 3;

            }
            else {

                SliderPokemon.Visibility = Visibility.Hidden;
            }
        }


      //  int counter_pkmn_max = +1;
      //  int counter_zero_max = 0;

        private async void ArrowL_Click(object sender, RoutedEventArgs e)
        {

           /* int pkmn_total = total * -1 + 2;
            counter_zero_max = pkmn_total;

            if (total != 3 && total != 2 && total != 1)
            {

                MessageBox.Show("" + counter_zero_max + "/" + pkmn_total);
                await getPokemonData(counter_zero_max);
                counter_pkmn_max = counter_pkmn_max + 1;
            }


            if (counter_pkmn_max == pkmn_total - 2)
            {
                counter_pkmn_max = 0;
            }*/
        }



        //   int counter_pkmn_minus = -1;
        //   int counter_zero_minus = 0;

        private async void ArrowR_Click(object sender, RoutedEventArgs e)
        {
            /*int pkmn_total = total * -1 + 2;
            counter_zero_minus--;

            if (total != 3 && total != 2 && total != 1)
            {

                MessageBox.Show("" + counter_zero_minus + "/" + pkmn_total);
                await getPokemonData(counter_pkmn_minus);
                counter_pkmn_minus = counter_pkmn_minus - 1;
            }

            if (counter_pkmn_minus == pkmn_total)
            {
                counter_pkmn_minus = 0;
            }*/

        }

        private async void TestSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

            double slider = SliderPokemon.Value;
            int x = (int)slider;

            int y = -1 * x;

            int comprovar = -1 * total + 2;

            if (total != 3 && total != 2 && total != 1)
            {
                await getPokemonData(y);
            } 
        }

        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }

        private void Button_MouseRight(object sender, RoutedEventArgs e)
        {
            Menu.Menu menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }
    }
}

