﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplicacióWPFPokemonGoOlot
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }

       

        public async Task getUserData()
        {
            var user = UserField.Text;
            var password = PasswordField.Password;
            var userInfo = await UserProcessor.LoadUser(user, password);
            if (userInfo != null)
            {
                if (user == userInfo.NomUsuari)
                {
                    //  MessageBox.Show("Login Correcte");
                    GlobalUsuari.set(user);
                    Menu.Menu menu = new();
                    menu.Show();
                    Visibility = Visibility.Hidden;

                }
            }else
            {
                MessageBox.Show("Error en el Login. Comprova l'usuari i contrasenya.");
            }
           
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (UserField.Text == "" || PasswordField.Password == "") {

                MessageBox.Show("Error en el Login, hi ha camps buits!");

            } else
            {
                await getUserData();
            }
        }

        private void goLogin(object sender, RoutedEventArgs e)
        {
            Register login = new Register();
            login.Show();
            Visibility = Visibility.Hidden;

        }

        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }
    }
}
