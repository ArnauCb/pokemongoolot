﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using System.Collections.Generic;


namespace AplicacióWPFPokemonGoOlot
{
    public class UserProcessor
    {
         public static string baseApi = $"http://172.24.4.244:5000/";
        //public static string baseApi = $"http://127.0.0.1:5000/";
        public static async Task<UserModel> LoadUser(string user, string password)
        {

            string url = "";
            url = baseApi +"usuari/login/" + user + "/" + password;


            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    UserModel model = await response.Content.ReadAsAsync<UserModel>();


                    return model;
                }
                else
                {
                   // throw new Exception(response.ReasonPhrase);
                    return null;
                }


            }
        }

        public static async Task<List<UserModel>> GetAllUser()
        {

            string url = "";
            url = baseApi + "usuari/getAll";


            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    List<UserModel> model = await response.Content.ReadAsAsync<List<UserModel>>();


                    return model;
                }
                else
                {
                    // throw new Exception(response.ReasonPhrase);
                    return null;
                }


            }
        }

        public static async Task<bool> AddUser(UserModel user)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseApi);
            HttpResponseMessage response = await client.PostAsJsonAsync("usuari/add", user);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
