﻿using System;

namespace AplicacióWPFPokemonGoOlot
{
    public class UserModel
    {
        public string NomUsuari { get; set; }
        public string Password { get; set; }
        public double? Nivell { get; set; }
        public DateTime? DataCreacio { get; set; }
        public double? Distancia { get; set; }
        public int? PolvosEstelares { get; set; }
        public bool? IsOnline { get; set; }
        public string NomEquip { get; set; }

    }
}
