﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Input;
using Microsoft.Maps.MapControl.WPF;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace AplicacióWPFPokemonGoOlot.Pokeradar
{
    public partial class Pokeradar
    {
        private static Socket clinetSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static List<Helpers.pokemonSalvatge> pokemonSalvatges = new List<Helpers.pokemonSalvatge>();

        public Pokeradar()
        {
            InitializeComponent();
            OpenConnection();
            GetPokemons();
            addPokemonInMap();
        }
        

        private static void GetPokemons()
        {
            try
            {
                byte[] request = Encoding.ASCII.GetBytes("pokemons");
                clinetSocket.Send(request);
                byte[] response = new byte[110000];
                int rec = clinetSocket.Receive(response);
                byte[] data = new byte[rec];
                Array.Copy(response, data, rec);
                string message = Encoding.ASCII.GetString(data);
                Console.WriteLine("ALL DATA");
                Console.WriteLine(data.Length);

                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(List<Helpers.pokemonSalvatge>));

                using (StringReader sr = new StringReader(message))
                {
                    pokemonSalvatges = (List<Helpers.pokemonSalvatge>)ser.Deserialize(sr);
                }
                Console.WriteLine("AQUI ");
                Console.WriteLine(pokemonSalvatges[0].NomPokemon);
                Console.WriteLine(pokemonSalvatges.Count);
            }
            catch (SocketException)
            {
                Console.WriteLine("ERROR");

            }
        }

        public static void OpenConnection() // ho separem per poder fer multiples connexions
        {
            while (!clinetSocket.Connected)
            {
                Console.WriteLine("HHE");
                clinetSocket.Connect(System.Net.IPAddress.Loopback, 4444);
            }
        }

        public void refreshPokemon(object sender, RoutedEventArgs e)
        {
            GetPokemons();
            addPokemonInMap();
        }

        public void addPokemonInMap()
        {
            {
                foreach (Helpers.pokemonSalvatge pokemon in pokemonSalvatges)
                {
                    Location pinLocation = new();
                    pinLocation.Latitude = pokemon.Latitud;
                    pinLocation.Longitude = pokemon.Longitud;

                    MapLayer mapLayer = new MapLayer();
                    Image myPushPin = new Image();
                    myPushPin.Source = new BitmapImage(new Uri(pokemon.ImgPokemon));
                    myPushPin.Width = 40;
                    myPushPin.Height = 40;
                    mapLayer.AddChild(myPushPin, pinLocation, PositionOrigin.Center);
                    myMap.Children.Add(mapLayer);
                }
            }
        }
        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }

        private void Button_MouseRight(object sender, RoutedEventArgs e)
        {
            Menu.Menu menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }
    }
}
