﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacióWPFPokemonGoOlot.PokedexUsuariInfo
{
    internal class PokedexUsuariModel
    {
        public int NumPokedex { get; set; }

        public string NomUsuari { get; set; }

        public DateTime DataVisualització { get; set; }

        public int? CopsAtrapat { get; set; }

      
    }
}
