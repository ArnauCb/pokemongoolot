﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;


namespace AplicacióWPFPokemonGoOlot.PokedexUsuariInfo
{
    internal class PokedexUsuariProcessor
    {
        public static string baseApi = $"http://172.24.4.244:5000/";
        //public static string baseApi = $"http://127.0.0.1:5000/";
        public static async Task<List<PokedexUsuariModel>> LoadPokedexUsuari(string Usuari)
        {

            string url = "";
            url = baseApi + "pokedexUsuari/getPokedexByUser/" + Usuari;


            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    List<PokedexUsuariModel> model = await response.Content.ReadAsAsync<List<PokedexUsuariModel>>();


                    return model;
                }
                else
                {
                    // throw new Exception("PAtata");
                    return null;
                }


            }
        }
    }
}
