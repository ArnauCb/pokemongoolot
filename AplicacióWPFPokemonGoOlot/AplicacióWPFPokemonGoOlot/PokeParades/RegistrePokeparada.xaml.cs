﻿using System;
using System.Windows;
using System.Windows.Input;
using Microsoft.Maps.MapControl.WPF;
using Microsoft.Win32;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Threading.Tasks;
using AplicacióWPFPokemonGoOlot.PokeParadesInfo;
using System.Collections.Generic;

using Point = System.Windows.Point;

namespace AplicacióWPFPokemonGoOlot.PokeParades
{
    public partial class RegistrePokeparada
    {
        public RegistrePokeparada()
        {
            InitializeComponent();
        }

        public  int indexNovaPokeparada = -1;

        private void MapWithPushpins_MouseDoubleClick(object sender, MouseButtonEventArgs e)             //=> LA LLIBRERIA https://docs.microsoft.com/en-us/previous-versions/bing/wpf-control/hh750210(v=msdn.10) 
        {
            if(indexNovaPokeparada != -1)
            {
                myMap.Children[indexNovaPokeparada].Visibility = Visibility.Hidden;
            }
           
            // Disables the default mouse double-click action.
            e.Handled = true;

            Point mousePosition = e.GetPosition(this);
            mousePosition.Y = mousePosition.Y - 308;
            mousePosition.X = mousePosition.X - 178;

            Location pinLocation = myMap.ViewportPointToLocation(mousePosition);

            MapLayer mapLayer = new MapLayer();
            Image myPushPin = new Image();
            myPushPin.Source = new BitmapImage(new Uri("https://raw.githubusercontent.com/PokeMiners/pogo_assets/master/Images/Pokestops%20and%20Gyms/img_pokestop.png"));
            myPushPin.Width = 65;
            myPushPin.Height = 65;
            mapLayer.AddChild(myPushPin, pinLocation, PositionOrigin.Center);
            myMap.Children.Add(mapLayer);

            indexNovaPokeparada = myMap.Children.IndexOf(mapLayer);

            longitud_map.Text = pinLocation.Longitude.ToString();
            latitud_map.Text = pinLocation.Latitude.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog getImage = new OpenFileDialog();
            getImage.InitialDirectory = @"C:\";
            getImage.Filter = "Fitxers d'imatge|*.jpg;*.png;*.jpeg;";
            getImage.FileName = "Imatge pokeparada";

            if (getImage.ShowDialog() == true)
            {
                MessageBox.Show("imatge carregada correctament");
            }
        }

        public async Task getPokeParada()
        {
            var nom = nom_pokeparada.Text;
            double longitud = Convert.ToDouble(longitud_map.Text);
            double latitud = Convert.ToDouble(latitud_map.Text);
            PokeParadaModel parada = new PokeParadaModel() { NomPokeparada = nom, Longitud = longitud, Latitud = latitud };
            var ParadaInfo = await PokeParadaProcessor.AddPokeParada(parada);

            if (ParadaInfo == true)
            {
                MessageBox.Show("Pokeparada enviada correctament");
                myMap.Children[indexNovaPokeparada].Visibility = Visibility.Hidden;
                indexNovaPokeparada = -1;
                nom_pokeparada.Text = "";
                longitud_map.Text = "";
                latitud_map.Text = "";
                //Afegir refreshPokeparades
            }
            else
            {
                MessageBox.Show("La pokeparada no s'ha pogut enviar");
            }
        }

        private async void sendParada(object sender, RoutedEventArgs e)
        {


            if (nom_pokeparada.Text == "" || latitud_map.Text == "" || longitud_map.Text == "")
            {
                MessageBox.Show("Omple els camps buits abans d'enviar la Poképarada.");
            } else { 
            
                await getPokeParada();
            
            }

            
        }

        private void MarcarUbi(object sender, RoutedEventArgs e)
        {

            if (indexNovaPokeparada != -1)
            {
                myMap.Children[indexNovaPokeparada].Visibility = Visibility.Hidden;
            }

            double lng = Convert.ToDouble(longitud_map.Text);
            double lat = Convert.ToDouble(latitud_map.Text);


            Location pinLocation = new();

            pinLocation.Longitude = lng;
            pinLocation.Latitude = lat;

            MapLayer create = new MapLayer();
            Image myPushPin = new Image();
            myPushPin.Source = new BitmapImage(new Uri("https://raw.githubusercontent.com/PokeMiners/pogo_assets/master/Images/Pokestops%20and%20Gyms/img_pokestop.png"));
            myPushPin.Width = 65;
            myPushPin.Height = 65;
            create.AddChild(myPushPin, pinLocation, PositionOrigin.Center);
            myMap.Children.Add(create);

            indexNovaPokeparada = myMap.Children.IndexOf(create);

        }

        private void clearPokeparades(object sender, RoutedEventArgs e)
        {
            int childrens = myMap.Children.Count;
            for(int i = 0; i < childrens; i++)
            {
                if (i != indexNovaPokeparada)
                {
                    myMap.Children[i].Visibility = Visibility.Hidden;
                }
            }
            clear.Visibility = Visibility.Hidden;
            show.Visibility = Visibility.Visible;
        }

        private async void showPokeparades(object sender, RoutedEventArgs e)
        {
                List<PokeParadaModelDB> ParadaInfo = await PokeParadaProcessor.GetAllPokeparades();

                foreach (PokeParadaModelDB pokeparada in ParadaInfo)
                {
                    Location pinLocation = new();
                    pinLocation.Latitude = pokeparada.Latitud;
                    pinLocation.Longitude = pokeparada.Longitud;

                    MapLayer mapLayer = new MapLayer();
                    Image myPushPin = new Image();
                    myPushPin.Source = new BitmapImage(new Uri("https://raw.githubusercontent.com/PokeMiners/pogo_assets/master/Images/Pokestops%20and%20Gyms/img_pokestop.png"));
                    myPushPin.Width = 40;
                    myPushPin.Height = 40;
                    mapLayer.AddChild(myPushPin, pinLocation, PositionOrigin.Center);
                    myMap.Children.Add(mapLayer);
                }
                show.Visibility = Visibility.Hidden;
                clear.Visibility = Visibility.Visible;
        }

        private void Button_MouseRight(object sender, RoutedEventArgs e)
        {
            Menu.Menu menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }

        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }
    }
}