﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplicacióWPFPokemonGoOlot
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Register : Window
    {
        public Register()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }

       

        private async Task addUser()
        {
           
           //    PasswordMal.Visibility = Visibility.Visible;
           //    UserMal.Visibility = Visibility.Visible;
           
                var user = UserRegister.Text;
                var password = Password1.Password;
                UserModel usuari = new UserModel() { NomUsuari = user, Password = password };
                var userInfo = await UserProcessor.AddUser(usuari);

                if (userInfo == true)
                {
                    MessageBox.Show("Registre Correcte");
                    goLoginPage();
                }
                else
                {
                    MessageBox.Show("L'usuari ja existeix ");
                }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (UserRegister.Text == "" || Password1.Password == "")
            {
                MessageBox.Show("Error en el Registre, hi ha camps buits!");
            }
            else
            {
                await addUser();
            }
        }

        private void goLogin(object sender, RoutedEventArgs e)
        {
            goLoginPage();            
        }
        public void goLoginPage()
        {
            Login login = new Login();
            login.Show();
            Visibility = Visibility.Hidden;
        }

        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }
    }
}
