﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplicacióWPFPokemonGoOlot.MenuUsuari
{
    /// <summary>
    /// Lógica de interacción para MenuUsuari.xaml
    /// </summary>
    public partial class MenuUsuari : Window
    {
        public MenuUsuari()
        {
            InitializeComponent();
        }
        private void goPokedex(object sender, RoutedEventArgs e)
        {
            PokedexUsuari.PokedexUsuari pokedex = new();
            pokedex.Show();
            Visibility = Visibility.Hidden;
        }

        private void goMapa(object sender, RoutedEventArgs e)
        {
            PokeParades.RegistrePokeparada registre = new();
            registre.Show();
            Visibility = Visibility.Hidden;
        }

        public void goPokemonUsuari(object sender, RoutedEventArgs e)
        {
            PokemonsUsuari.PokemonsUsuari pokemonUsuari = new();
            pokemonUsuari.Show();
            Visibility = Visibility.Hidden;
        }

        private void Click_Back(object sender, RoutedEventArgs e)
        {
            Menu.Menu menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }

        private void goGestionar(object sender, RoutedEventArgs e)
        {
            var user = GlobalUsuari.get();

            if (user == "admin")
            {
                GestionarPokemonsUsuaris.GestionarPokemonsUsuari gestio = new();
                gestio.Show();
                Visibility = Visibility.Hidden;
            }
            else
            {
                MessageBox.Show("Has de ser usuari administrador");
            }

            
        }

        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }
    }
}
