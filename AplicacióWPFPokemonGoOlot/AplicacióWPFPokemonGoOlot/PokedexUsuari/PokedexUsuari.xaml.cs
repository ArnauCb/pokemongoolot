﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AplicacióWPFPokemonGoOlot.PokedexUsuariInfo;


namespace AplicacióWPFPokemonGoOlot.PokedexUsuari
{
    /// <summary>
    /// Lógica de interacción para PokedexUsuari.xaml
    /// </summary>
    public partial class PokedexUsuari : Window
    {
        public PokedexUsuari()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();

            getPokedexUsuari();
        }
        public async Task getPokedexUsuari()
        {
            var user = GlobalUsuari.get();
            if (user != null)
            {
                List<PokedexUsuariModel> List = await PokedexUsuariProcessor.LoadPokedexUsuari(user);
                PokedexPokemons.ItemsSource = List;

            }


        }
        private void Button_MouseRight(object sender, RoutedEventArgs e)
        {
            MenuUsuari.MenuUsuari menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }

        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }
    
}
}
