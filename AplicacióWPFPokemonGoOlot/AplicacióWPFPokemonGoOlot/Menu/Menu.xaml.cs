﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplicacióWPFPokemonGoOlot.Menu
{
    /// <summary>
    /// Lógica de interacción para Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void goEvolution(object sender, RoutedEventArgs e)
        {
            Evolució.Evolucio evolution = new();
            evolution.Show();
            Visibility = Visibility.Hidden;
        }

        private void goMenuUsuari(object sender, RoutedEventArgs e)
        {
            MenuUsuari.MenuUsuari usuari = new();
            usuari.Show();
            Visibility = Visibility.Hidden;
        }

        private void goMapa(object sender, RoutedEventArgs e)
        {
            PokeParades.RegistrePokeparada registre = new();
            registre.Show();
            Visibility = Visibility.Hidden;
        }

        public void goPokeradar(object sender, RoutedEventArgs e)
        {
            Pokeradar.Pokeradar pokeradar = new();
            pokeradar.Show();
            Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Próximament.");
        }

        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }
    }
}
