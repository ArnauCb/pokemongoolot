﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AplicacióWPFPokemonGoOlot.PokemonsUsuariInfo;


namespace AplicacióWPFPokemonGoOlot.GestionarPokemonsUsuaris
{
    /// <summary>
    /// Lógica de interacción para GestionarPokemonsUsuari.xaml
    /// </summary>
    public partial class GestionarPokemonsUsuari : Window
    {
        public GestionarPokemonsUsuari()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
            getUsuaris();

        }
        public async Task getPokemonsUsuari(string user)
        {
            if (user != null)
            {
                List<PokemonsUsuariModel> List = await PokemonsUsuariProcessor.LoadPokemonsUsuari(user);
                llistaPokemons.ItemsSource = List;

            }

        }

        public async Task getUsuaris()
        {
            List<UserModel> List = await UserProcessor.GetAllUser();
            llistaUsuaris.ItemsSource = List;
        }
        private void Button_MouseRight(object sender, RoutedEventArgs e)
        {
            MenuUsuari.MenuUsuari menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }

        private void RefrescarLLista(object sender, RoutedEventArgs e)

        {
            var user = llistaUsuaris.SelectedItem as UserModel;
            string nomUsuari = user.NomUsuari.ToString();
            getPokemonsUsuari(nomUsuari);
        }

        private async void EditarPokemon(object sender, RoutedEventArgs e)
        {
            var cellInfos = llistaPokemons.SelectedCells[0];

            int content = Convert.ToInt32((cellInfos.Column.GetCellContent(cellInfos.Item) as TextBlock).Text);

            //await DeletePokemonUsuari(content);






        }


        private async void EliminarPokemon(object sender, RoutedEventArgs e)
        {
            var cellInfos = llistaPokemons.SelectedCells[0];
           
            int content = Convert.ToInt32((cellInfos.Column.GetCellContent(cellInfos.Item) as TextBlock).Text);

            await DeletePokemonUsuari(content);
            

        }



        public async Task EditPokemonUsuari(int id)
        {


            var EdPokemonUsuariInfo = await PokemonsUsuariProcessor.DelPokemonUser(id);

            if (EdPokemonUsuariInfo == true)
            {
                MessageBox.Show("Pokemon editat correctament");

            }
            else
            {
                MessageBox.Show("El Pokemon no s'ha pogut editar");
            }
        }




        public async Task DeletePokemonUsuari(int id)
        {
            var DelPokemonUsuariInfo = await PokemonsUsuariProcessor.DelPokemonUser(id);

            if (DelPokemonUsuariInfo == true)
            {
                MessageBox.Show("Pokemon eliminat correctament");
                
            }
            else
            {
                MessageBox.Show("El Pokemon no s'ha pogut eliminar");
            }
        }

        private void goCrearPokemon(object sender, RoutedEventArgs e)
        {
            CrearPokemonUsuari.Window1 crear = new ();
            crear.Show();
            Visibility = Visibility.Hidden;
        }

        private void goEditarPokemon(object sender, RoutedEventArgs e)
        {
            var cellInfos = llistaPokemons.SelectedCells[0];

            var content = Convert.ToInt32((cellInfos.Column.GetCellContent(cellInfos.Item) as TextBlock).Text);

            GlobalPokemonUsuari.set(content);
            EditarPokemonUsuari.EditarPokemonUsuari editar = new();
            editar.Show();
            Visibility = Visibility.Hidden;
        }


        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }

       
    }
}
