﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AplicacióWPFPokemonGoOlot.PokemonsUsuariInfo;


namespace AplicacióWPFPokemonGoOlot.PokemonsUsuari
{
    /// <summary>
    /// Lógica de interacción para PokemonsUsuari.xaml
    /// </summary>
    public partial class PokemonsUsuari : Window
    {
        public PokemonsUsuari()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();

            getPokemonsUsuari();

        }
      

        public async Task getPokemonsUsuari()
        {
            var user = GlobalUsuari.get();
            if (user != null)
            {
                List<PokemonsUsuariModel> List = await PokemonsUsuariProcessor.LoadPokemonsUsuari(user);
                llistaPokemons.ItemsSource = List;

            }


        }
        private void Button_MouseRight(object sender, RoutedEventArgs e)
        {
            MenuUsuari.MenuUsuari menu = new();
            menu.Show();
            Visibility = Visibility.Hidden;
        }

        private void Click_Cross(object sender, RoutedEventArgs e)

        {
            App.Current.Shutdown();

        }
        private void Click_Minus(object sender, RoutedEventArgs e)

        {

            WindowState = WindowState.Minimized;
        }
    }
}
