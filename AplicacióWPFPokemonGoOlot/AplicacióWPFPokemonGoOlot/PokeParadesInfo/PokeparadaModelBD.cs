﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AplicacióWPFPokemonGoOlot.PokeParadesInfo
{
    public class PokeParadaModelDB
    {
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public string NomPokeparada { get; set; }
        public string DescripcioPokeparada { get; set; }
        public string ImgPokeparada { get; set; }
        public string TipusPokeparada { get; set; }
    }
}
