﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AplicacióWPFPokemonGoOlot.PokeParadesInfo
{
    public class PokeParadaModel
    {
        public string NomPokeparada { get; set; }
        public double Longitud { get; set; }
        public double Latitud { get; set; }
        public IFormFile ImgPokeparada { get; set; }
    }
}
