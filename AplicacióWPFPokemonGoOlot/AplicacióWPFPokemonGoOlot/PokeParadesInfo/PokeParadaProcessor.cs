﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AplicacióWPFPokemonGoOlot.PokeParadesInfo
{
    public class PokeParadaProcessor
    {
        public static string baseApi = $"http://172.24.4.244:5000/";
        //public static string baseApi = $"http://127.0.0.1:5000/";
         

        public static async Task<bool> AddPokeParada(PokeParadaModel parada)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseApi);
            HttpResponseMessage response = await client.PostAsJsonAsync("pokeparada/add", parada);
            return response.IsSuccessStatusCode;
       
        }

        public static async Task<List<PokeParadaModelDB>> GetAllPokeparades()
        {
            string  url = baseApi + "pokeparada/getAll";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseApi);

            HttpResponseMessage response = await client.GetAsync(url);
            
                if (response.IsSuccessStatusCode)
                {
                    List<PokeParadaModelDB> model = await response.Content.ReadAsAsync<List<PokeParadaModelDB>>();

                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
        }
    }
}
