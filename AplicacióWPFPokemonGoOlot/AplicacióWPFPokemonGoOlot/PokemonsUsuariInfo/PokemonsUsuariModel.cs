﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacióWPFPokemonGoOlot.PokemonsUsuariInfo
{
    internal class PokemonsUsuariModel
    {
        public int IdUsuariPokemon { get; set; }

        public string NomUsuari { get; set; }

        public DateTime DataCaptura { get; set; }

        public int? NumPokedex { get; set; }

        public int? Pc { get; set; }

        public int? Ps { get; set; }

        public string HabilitatPokemonSimple { get; set; }

        public string HabilitatPokemonCarregat { get; set; }

        //public bool IsInGym { get; set; }
    }
}
