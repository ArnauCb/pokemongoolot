﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;


namespace AplicacióWPFPokemonGoOlot.PokemonsUsuariInfo
{
    internal class PokemonsUsuariProcessor
    {
        public static string baseApi = $"http://172.24.4.244:5000/";
        //public static string baseApi = $"http://127.0.0.1:5000/";
        public static async Task<List<PokemonsUsuariModel>> LoadPokemonsUsuari(string Usuari)
        {

            string url = "";
            url = baseApi + "pokemonUsuari/getAllPokemonsByUser/" + Usuari;


            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    List<PokemonsUsuariModel> model = await response.Content.ReadAsAsync<List<PokemonsUsuariModel>>();


                    return model;
                }
                else
                {
                    // throw new Exception("PAtata");
                    return null;
                }


            }
        }

        public static async Task<bool> AddPokemonUser(PokemonsUsuariModel pokemonUser)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseApi);
            HttpResponseMessage response = await client.PostAsJsonAsync("pokemonUsuari/createPokemon/", pokemonUser);
            return response.IsSuccessStatusCode;

        }

        public static async Task<bool> DelPokemonUser(int idPokemonUser)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseApi);
            HttpResponseMessage response = await client.DeleteAsync("pokemonUsuari/delete/" + idPokemonUser);
            return response.IsSuccessStatusCode;

        }

      

        public static async Task<List<PokemonsUsuariModel>> GetInfoUser(int idPokemonUser)
        {
            string url = "";
            url = baseApi + "pokemonUsuari/info/" + idPokemonUser;


            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    List<PokemonsUsuariModel> model = await response.Content.ReadAsAsync<List<PokemonsUsuariModel>>();


                    return model;
                }
                else
                {
                    // throw new Exception(response.ReasonPhrase);
                    return null;
                }


            }

        }
       




    }
}
