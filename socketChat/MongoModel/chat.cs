using System;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace socketChat.MongoModel
{
    [Serializable]
    public class chat
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id {get; set;}
        
        [BsonElement("emisor")] //=> nom del camp
        public string Emisor {get;set;}
        
        [BsonElement("receptor")] //=> nom del camp
        public string Receptor {get;set;}
        
        [BsonElement("missatge")] //=> nom del camp
        public string Missatge {get;set;}
    }
}