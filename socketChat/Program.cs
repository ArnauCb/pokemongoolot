﻿using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Connexio;
using System.Timers;
using System.Xml.Serialization;
using System.Xml;
using MongoDB.Driver;
using socketChat.MongoModel;


namespace socketPokemonGoOlot
{
    class Program
    {
        private static Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); //Socket principal params
        private static readonly List<Socket> clientSockets = new List<Socket>(); // Llista de clients
        private const int BUFFER_SIZE = 2048; // mida max dels missatges 
        private const int PORT = 4444;
        private static byte[] buffer = new byte[BUFFER_SIZE]; //aqui arriba el missatge del client
        // Timer vairable
        private static System.Timers.Timer aTimer;
        
        // DATABASES variables
        public static pokemongoolotContext dbContext = new pokemongoolotContext();
        public static List<Usuari> User = new List<Usuari>();
        public static MongoClient client = new MongoClient("mongodb+srv://user:Patata44@cluster0.anjj9.mongodb.net/test");  
        static void Main(string[] args)
        {
           
            SetUp();
            Console.ReadLine();
            // aTimer.Stop(); // reset pokemons finish
            // aTimer.Dispose();
        }

        private static void SetUp()
        {
            Console.WriteLine("Servidor escoltant el port : " + PORT + " IP: "+ IPAddress.Loopback );
            serverSocket.Bind(new IPEndPoint(IPAddress.Loopback, PORT));
            serverSocket.Listen(10); // Peticions abans de tencar
            serverSocket.BeginAccept(AcceptCallback, null);
            Console.WriteLine("SERVER READY BEEEEECCCCCHHHH");
        }
        private static void AcceptCallback(IAsyncResult data)
        {
            Socket socket;
            try
            {
                socket = serverSocket.EndAccept(data);
            }
            catch (ObjectDisposedException)
            {
                Console.WriteLine("Connexion failed");
                return;
            }
            clientSockets.Add(socket); // ho afegim a la llista de clients
            socket.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, AdrecaCallback, socket);
            serverSocket.BeginAccept(AdrecaCallback, null); //Aqui accepta la peticio i crida a la funcio
        }

        private static void AdrecaCallback(IAsyncResult data)
        {
            Socket curent = (Socket)data.AsyncState; // agafem el client que demana la peticio
            int messageSize;

            try
            {
                messageSize = curent.EndReceive(data);
            }
            catch (SocketException)
            {
                Console.WriteLine("DISCONECTED");
                clientSockets.Remove(curent); // es treu de la llista de clients pk la peticio ha fallat
                curent.Close();
                return;
            }

            byte[] recive = new byte[messageSize];
            Array.Copy(buffer, recive, messageSize); // copiem la resposta per no alterar l'original (l'original hauria de ser readOnly)
            chat chatRecive = ByteArrayToObject(recive);

            Console.WriteLine("PROVA CHAT");
            Console.WriteLine(chatRecive.Missatge);
            
            string path = Encoding.ASCII.GetString(recive);
            switch (path)
            { // AQUI ES ON ES GENEREN LES PETICIONS I EL QUE ENVIAR
                
                case "get":
                    byte[] prova = Encoding.ASCII.GetBytes("Prova Conex");
                    sendData(prova, curent);
                    break;
                default:
                    Console.WriteLine("PATH NO IMPLEMENTAT");
                    break;
            }

            //Reactivem la peticio pk sino es tencaria a la primera
            curent.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, AdrecaCallback, curent);
        }

    
            
        private static void sendData(byte[] data, Socket curent)
        {
            // Console.WriteLine(Encoding.ASCII.GetString(data));
            Console.WriteLine(data.Length);
            curent.Send(data); //Enviem les dades                 
        }

       
        private static byte[] ObjectToByteArray(Object obj)
        {
            if(obj == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        // Convert a byte array to an Object
        private static chat ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            chat obj = (chat) binForm.Deserialize(memStream);

            return obj;
        }
    }
}


