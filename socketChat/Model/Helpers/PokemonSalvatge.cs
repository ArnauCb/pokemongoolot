﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Connexio
{
    public partial class pokemonSalvatge
    {
        public int NumPokedex { get; set; }
        public string NomPokemon { get; set; }
        public string ImgPokemon { get; set; }
        public string TipusPokemon { get; set; }
        public int? PsMax { get; set; }
        public int? PcMax { get; set; }
        public int? BaseDefense {get ; set;}
        public int? BaseAttack {get ; set;}
        public double Latitud {get; set;}
        public double Longitud {get; set;}

    }
}
