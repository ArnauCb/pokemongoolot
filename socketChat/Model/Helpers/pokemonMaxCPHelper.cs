namespace Connexio
{
    public class pokemonMaxCPHelper
    {
        public string form { get; set; }
        public int max_cp { get; set; }
        public int pokemon_id { get; set; }
        public string pokemon_name { get; set; }
    }

}